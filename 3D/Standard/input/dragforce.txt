sizeX 340	
sizeY 340
sizeZ 340
timesteps 100
dt 0.001
dx 0.001
tau 1.5
GravityX 0.00
GravityY 0.0
GravityZ 0.0
ForceX 0.0
ForceY 0.0
ForceZ 0.0

################## INIT
initPressure 1.0
initVelocityX_L 0.0
initVelocityY_L 0.0
initVelocityZ_L 0.0

################### OUTPUT
OutputVelocity NULL
OutputDensity NULL
OutputFlag NULL
OutputDrag 340_340_340_1.5

printstep 1
tracestep 10

################### BOUNDARY
Top    ZOU_HE_TOP_PRESSURE 0.0 0.0 0.0 1.0
Bottom ZOU_HE_BOTTOM_VELOCITY 0.0 0.0 0.0 0.0066666667
North  FREE_SLIP_N_S
South  FREE_SLIP_N_S
East   FREE_SLIP_E_W
West   FREE_SLIP_E_W

################### EDGES
TopNorth     FREE_SLIP_N_S
TopSouth     FREE_SLIP_N_S
TopEast      FREE_SLIP_E_W
TopWest      FREE_SLIP_E_W
BottomNorth  FREE_SLIP_N_S
BottomSouth  FREE_SLIP_N_S
BottomEast   FREE_SLIP_E_W
BottomWest   FREE_SLIP_E_W
NorthEast    NO_SLIP
NorthWest    NO_SLIP
SouthEast    NO_SLIP
SouthWest    NO_SLIP

################### CORNERS
TopNorthEast    NO_SLIP
TopNorthWest    NO_SLIP
TopSouthEast    NO_SLIP
TopSouthWest    NO_SLIP
BottomNorthEast NO_SLIP
BottomNorthWest NO_SLIP
BottomSouthEast NO_SLIP
BottomSouthWest NO_SLIP



