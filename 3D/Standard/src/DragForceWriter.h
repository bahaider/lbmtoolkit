#include <libgeodecomp/config.h>
#ifdef LIBGEODECOMP_FEATURE_MPI
#ifndef _DRAG_FORCE_WRITER_h_
#define _DRAG_FORCE_WRITER_h_

#include <fstream>
#include <iomanip>

#include <libgeodecomp/io/mpiio.h>
#include <libgeodecomp/io/parallelwriter.h>
#include <libgeodecomp/mpilayer/typemaps.h>

namespace LibGeoDecomp {

/**
 * writes simulation snapshots compatible with VisIt's Brick of Values
 * (BOV) format using MPI-IO. Uses a selector which maps a cell to a
 * primitive data type so that it can be fed into VisIt.
 */
template<typename CELL_TYPE, typename SELECTOR_TYPE>
class DragForceWriter : public ParallelWriter<CELL_TYPE>
{
public:
//    friend class BOVWriterTest;

    typedef typename CELL_TYPE::Topology Topology;
    typedef typename SELECTOR_TYPE::VariableType VariableType;

    static const int DIM = CELL_TYPE::Topology::DIMENSIONS;

    using ParallelWriter<CELL_TYPE>::period;
    using ParallelWriter<CELL_TYPE>::prefix;

    DragForceWriter(
        const std::string& prefix, 
        const unsigned period, 
        const Coord<3>& brickletDim = Coord<3>(),
        const MPI::Intracomm& communicator = MPI::COMM_WORLD,
        MPI::Datatype mpiDatatype = Typemaps::lookup<VariableType>()) :
        ParallelWriter<CELL_TYPE>(prefix, period),
        brickletDim(brickletDim),
        comm(communicator),
        datatype(mpiDatatype)
    {}

    virtual void stepFinished(
        const typename ParallelWriter<CELL_TYPE>::GridType& grid, 
        const Region<Topology::DIMENSIONS>& validRegion, 
        const Coord<Topology::DIMENSIONS>& globalDimensions,
        unsigned step, 
        WriterEvent event, 
        bool lastCall)
    {
        if ((event == WRITER_STEP_FINISHED) && (step % period != 0)) {
            return;
        }
    
        writeRegion(step, globalDimensions, grid, validRegion);
    }


private:
    Coord<3> brickletDim;
    MPI::Intracomm comm;
    MPI::Datatype datatype;

    std::string filename(const unsigned& step, const std::string& suffix) const 
    {
        std::ostringstream buf;
        buf << prefix << "." << std::setfill('0') << std::setw(5) << step << "." << suffix;
        return buf.str();
    }

    template<typename GRID_TYPE>
    void writeRegion(
        const unsigned& step, 
        const Coord<DIM>& dimensions, 
        const GRID_TYPE& grid, 
        const Region<DIM>& region)
    {
        int dataComponents = SELECTOR_TYPE::dataComponents();
        SuperVector<VariableType> buffer(dataComponents);
        SuperVector<VariableType> temp(dataComponents);
        for( int d = 0; d < dataComponents; ++d ){
            buffer[d] = 0.0;
            temp[d] = 0.0;
        }

        for (typename Region<DIM>::StreakIterator i = region.beginStreak();
             i != region.endStreak();
             ++i) {
            int length = i->endX - i->origin.x();
            int effectiveLength = length * dataComponents;
            Coord<DIM> walker = i->origin;
            for (int i = 0; i < effectiveLength; i += dataComponents) {
            	SELECTOR_TYPE()(grid.at(walker), &buffer[0]);
                temp[0] = temp[0] + buffer[0];
                temp[1] = temp[1] + buffer[1];
                temp[2] = temp[2] + buffer[2];
                walker.x()++;
            }
        }
            
        double ergX = 88.88;
        double ergY = 33.99;
        double ergZ = 55.22;
        MPI_Reduce(&temp[0], &ergX, 1, MPI_DOUBLE, MPI_SUM,0,MPI_COMM_WORLD);
        MPI_Reduce(&temp[1], &ergY, 1, MPI_DOUBLE, MPI_SUM,0,MPI_COMM_WORLD);
        MPI_Reduce(&temp[2], &ergZ, 1, MPI_DOUBLE, MPI_SUM,0,MPI_COMM_WORLD);
	
        if(MPILayer().rank() == 0){
            std::ofstream outfile(prefix.c_str(),ios::out|ios::app);
            if(!outfile){
                std::cerr << "Can't do it....." << std::endl;
            }
            outfile << ergX << " " << ergY << " " << ergZ << std::endl; 
            outfile.close();
        }
    }
};

}

#endif
#endif
