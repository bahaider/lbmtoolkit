#ifndef CONSTANTS_H
#define CONSTANTS_H

#include<string>

enum {
	C = 0,
	N = 1,
	S = 2,
	W = 3,
	E = 4,
	T = 5,
	B = 6,
	NW = 7,
	NE = 8,
	SW = 9,
	SE = 10,
	TN = 11,
	TS = 12,
	TW = 13,
	TE = 14,
	BN = 15,
	BS = 16,
	BW = 17,
	BE = 18,
	CELLSIZE = 19
};

enum State {
	FLUID                             =  0,
	NO_SLIP                           =  1,
	OBSTACLE                          =  2, 
        FREE_SLIP_E_W                     =  3,
        FREE_SLIP_N_S                     =  4,
        FREE_SLIP_T_B                     =  5,
	ZOU_HE_BOTTOM_VELOCITY            =  6,
	ZOU_HE_TOP_VELOCITY               =  7,
        ZOU_HE_SOUTH_VELOCITY             =  8,
        ZOU_HE_NORTH_VELOCITY             =  9,
        ZOU_HE_EAST_VELOCITY              = 10,
        ZOU_HE_WEST_VELOCITY              = 11,
	ZOU_HE_BOTTOM_PRESSURE            = 12,
	ZOU_HE_TOP_PRESSURE               = 13,
        ZOU_HE_SOUTH_PRESSURE             = 14,
        ZOU_HE_NORTH_PRESSURE             = 15,
        ZOU_HE_EAST_PRESSURE              = 16,
        ZOU_HE_WEST_PRESSURE              = 17,
	ACCELERATION                      = 18,
        CORNER_BOTTOM_SOUTH_WEST_VELOCITY = 19,
        CORNER_BOTTOM_SOUTH_EAST_VELOCITY = 20,
        CORNER_BOTTOM_NORTH_WEST_VELOCITY = 21,
        CORNER_BOTTOM_NORTH_EAST_VELOCITY = 22,
        CORNER_TOP_SOUTH_WEST_VELOCITY    = 23,
        CORNER_TOP_SOUTH_EAST_VELOCITY    = 24,
        CORNER_TOP_NORTH_WEST_VELOCITY    = 25,
        CORNER_TOP_NORTH_EAST_VELOCITY    = 26,
        CORNER_BOTTOM_SOUTH_WEST_PRESSURE = 27,
        CORNER_BOTTOM_SOUTH_EAST_PRESSURE = 28,
        CORNER_BOTTOM_NORTH_WEST_PRESSURE = 29,
        CORNER_BOTTOM_NORTH_EAST_PRESSURE = 30,
        CORNER_TOP_SOUTH_WEST_PRESSURE    = 31,
        CORNER_TOP_SOUTH_EAST_PRESSURE    = 32,
        CORNER_TOP_NORTH_WEST_PRESSURE    = 33,
        CORNER_TOP_NORTH_EAST_PRESSURE    = 34,
        EDGE_BOTTOM_WEST_VELOCITY         = 35,
        EDGE_BOTTOM_EAST_VELOCITY         = 36,
        EDGE_BOTTOM_SOUTH_VELOCITY        = 37,
        EDGE_BOTTOM_NORTH_VELOCITY        = 38,
        EDGE_TOP_WEST_VELOCITY            = 39,
        EDGE_TOP_EAST_VELOCITY            = 40,
        EDGE_TOP_SOUTH_VELOCITY           = 41,
        EDGE_TOP_NORTH_VELOCITY           = 42,
        EDGE_SOUTH_WEST_VELOCITY          = 43,
        EDGE_SOUTH_EAST_VELOCITY          = 44,
        EDGE_NORTH_WEST_VELOCITY          = 45,
        EDGE_NORTH_EAST_VELOCITY          = 46,
        EDGE_BOTTOM_WEST_PRESSURE         = 47,
        EDGE_BOTTOM_EAST_PRESSURE         = 48,
        EDGE_BOTTOM_SOUTH_PRESSURE        = 49,
        EDGE_BOTTOM_NORTH_PRESSURE        = 50,
        EDGE_TOP_WEST_PRESSURE            = 51,
        EDGE_TOP_EAST_PRESSURE            = 52,
        EDGE_TOP_SOUTH_PRESSURE           = 53,
        EDGE_TOP_NORTH_PRESSURE           = 54,
        EDGE_SOUTH_WEST_PRESSURE          = 55,
        EDGE_SOUTH_EAST_PRESSURE          = 56,
        EDGE_NORTH_WEST_PRESSURE          = 57,
        EDGE_NORTH_EAST_PRESSURE          = 58,
	PERIODICITY                       = 59,
	DUMMY                             = 60
};


//                        C   N   S   W   E   T   B  NW  NE  SW  SE  TN  TS  TW  TE  BN  BS  BW  BE
const int g_cx[]       = {0,  0,  0, -1,  1,  0,  0, -1,  1, -1,  1,  0,  0, -1,  1,  0,  0, -1,  1 };
const int g_cy[]       = {0,  1, -1,  0,  0,  0,  0,  1,  1, -1, -1,  1, -1,  0,  0,  1, -1,  0,  0 };
const int g_cz[]       = {0,  0,  0,  0,  0,  1, -1,  0,  0,  0,  0,  1,  1,  1,  1, -1, -1, -1, -1 };
const int g_stream[]   = {C,  S,  N,  E,  W,  B,  T, SE, SW, NE, NW, BS, BN, BE, BW, TS, TN, TE, TW };
//====================================================================================================
const int g_freeSlip[] = {C,  S,  N,  E,  W,  B,  T, SW, SE, NW, NE, BN, BS, BW, BE, TN, TS, TW, TE };
//                        C   N   S   W   E   T   B  NW  NE  SW  SE  TN  TS  TW  TE  BN  BS  BW  BE
const int g_freeX[]    = {C,  N,  S,  E,  W,  T,  B, NE, NW, SE, SW, TN, TS, TE, TW, BN, BS, BE, BW };
const int g_freeY[]    = {C,  S,  N,  W,  E,  T,  B, SW, SE, NW, NE, TS, TN, TW, TE, BS, BN, BW, BE };
const int g_freeZ[]    = {C,  N,  S,  W,  E,  B,  T, NW, NE, SW, SE, BN, BS, BW, BE, TN, TS, TW, TE };
//                        C   N   S   W   E   T   B  NW  NE  SW  SE  TN  TS  TW  TE  BN  BS  BW  BE
const int g_pullX[]    = {0,  0,  0,  1, -1,  0,  0,  1, -1,  1, -1,  0,  0,  1, -1,  0,  0,  1, -1 };
const int g_pullY[]    = {0, -1,  1,  0,  0,  0,  0, -1, -1,  1,  1, -1,  1,  0,  0, -1,  1,  0,  0 };
const int g_pullZ[]    = {0,  0,  0,  0,  0, -1,  1,  0,  0,  0,  0, -1, -1, -1, -1,  1,  1,  1,  1 };
const double g_weight[]  = { 1./3,
                             1./18, 1./18, 1./18, 1./18, 1./18, 1./18,
                             1./36, 1./36, 1./36, 1./36, 1./36, 1./36,
                             1./36, 1./36, 1./36, 1./36, 1./36, 1./36 };

const std::string g_filename = "../input/params.txt";

#endif
