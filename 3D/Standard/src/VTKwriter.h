#include <libgeodecomp/config.h>
#ifdef LIBGEODECOMP_FEATURE_MPI
#ifndef VTK_WRITER_H
#define VTK_WRITER_H

#include <string>
#include <cerrno>
#include <fstream>
#include <iomanip>
#include <libgeodecomp/parallelization/simulator.h>
#include <libgeodecomp/io/image.h>
#include <libgeodecomp/io/ioexception.h>
#include <libgeodecomp/io/plotter.h>
#include <libgeodecomp/io/writer.h>


#define VARNAME 		ATTRIBUTE_SELECTOR::varName()
#define DATAFORMAT		ATTRIBUTE_SELECTOR::dataFormat()
#define DATATYPE 		ATTRIBUTE_SELECTOR::dataType()
#define DATACOMPONENTS		ATTRIBUTE_SELECTOR::dataComponents()

namespace LibGeoDecomp {

/**
 * An output plugin for writing text files. Uses the same selector
 * infrastucture as the BOVWriter.
 */
template<typename CELL_TYPE, typename ATTRIBUTE_SELECTOR>
class VTKWriter : public Writer<CELL_TYPE>
{    
public:
    typedef typename Writer<CELL_TYPE>::GridType GridType;
    static const int DIM = CELL_TYPE::Topology::DIMENSIONS;

    using Writer<CELL_TYPE>::period;
    using Writer<CELL_TYPE>::prefix;

    VTKWriter(
        const std::string& prefix,  
        const unsigned& period =1) :
        Writer<CELL_TYPE>(prefix, period)
    {}

    virtual void stepFinished(const GridType& grid, unsigned step, WriterEvent event)
    {

        if ((event == WRITER_STEP_FINISHED) && (step % period != 0)){
             return;
        }
        Coord<DIM> size = grid.getDimensions();
        std::ostringstream filename;
        filename << prefix << "." << std::setfill('0') << std::setw(6) << step << ".vtk";
        std::ofstream outfile(filename.str().c_str());
        if (!outfile) {
            throw FileOpenException(filename.str());
        }
        
    	outfile << "# vtk DataFile Version 4.0" << std::endl;
    	outfile << "Visualisation LBM" << std::endl;
    	outfile << "ASCII" << std::endl;
    	outfile << "DATASET STRUCTURED_POINTS" << std::endl;
    	outfile << "DIMENSIONS " << size.x() << " " << size.y() << " " << size.z() << std::endl;
    	outfile << "ORIGIN 0 0 0" << std::endl;
    	outfile << "SPACING 1 1 1" << std::endl;
    	outfile << "POINT_DATA " << size.x()*size.y()*size.z() << std::endl;
    	outfile << "\n" <<  DATATYPE << " " << VARNAME << " " << DATAFORMAT <<  std::endl;
    //	outfile << "LOOKUP_TABLE default" << std::endl;
        
        SuperVector<double> buffer(DATACOMPONENTS, 77.7);//Dummy value for test cases

        CoordBox<DIM> box = grid.boundingBox();
        for (typename CoordBox<DIM>::Iterator i = box.begin(); i != box.end(); ++i) {
        	
        	ATTRIBUTE_SELECTOR()( (grid)[*i], &buffer[0] );
        	for( int d = 0; d < DATACOMPONENTS; ++d ){
        		outfile << setprecision(18) << buffer[d] << " ";
        	}
        	outfile << std::endl;
      
        }

        if (!outfile.good()) {
            throw FileWriteException(filename.str());
        }
        outfile.close();
    }
};

}

#endif
#endif
