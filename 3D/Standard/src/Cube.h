#ifndef _CUBE_H_
#define _CUBE_H_

#include "Geometry.h"

class Cube : public Geometry
{
   private:
       int m_half_l;
       int m_half_l_B;
        
   public:
       Cube(int mx, int my, int mz, int l) : Geometry(mx,my,mz)
       {
           m_half_l = l/2.;
           m_half_l_B = (l+2.)/2.;         
       }
       
       bool IsInside(int x, int y, int z)
       {
           int xn = abs(x-m_X);
           int yn = abs(y-m_Y);
           int zn = abs(z-m_Z);
           if(xn <= m_half_l && yn <= m_half_l && zn <= m_half_l){
               return 1;
           }
           else{ return 0; }           
       }   

       bool IsBoundingBox(int x, int y, int z)
       {
           int xn = abs(x-m_X);
           int yn = abs(y-m_Y);
           int zn = abs(z-m_Z);
           if(xn <= m_half_l_B && yn <= m_half_l_B && zn <= m_half_l_B){
               return 1;
           }

           else{ return 0; }           
       }   
};

#endif
