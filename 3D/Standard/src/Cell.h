#ifndef _CELL_H_
#define _CELL_H_

#include "Constants.h"
#include "Provider.h"
#include "math.h"

#define GET_STATE(X, Y, Z) neighborhood[Coord<3>(X, Y, Z)].state
#define GET_COMP(X, Y, Z, COMP) neighborhood[Coord<3>(X, Y, Z)].comp[COMP]
#define NEIGH(X,Y,Z) neighborhood[Coord<3>(X,Y,Z)]
#define SQR(X) ((X)*(X))

using namespace LibGeoDecomp;

class Cell
{
  
public:
    typedef Stencils::Moore<3, 1> Stencil;
    typedef Topologies::Torus<3>::Topology Topology;
    class API : public CellAPITraits::Base
    {};

    static inline unsigned nanoSteps()
    {
        return 1;
    }

    inline explicit Cell(const State& s = DUMMY, const int& px = 0, const int& py = 0, const int& pz = 0, const int& bb = 0 ) :
	state(s), bounceback(bb), dragX(0.0), dragY(0.0), dragZ(0.0)
	{
            density   = provider.GetInitPressure();
            velocityX = provider.GetInitVelocityX();
            velocityY = provider.GetInitVelocityY();
            velocityZ = provider.GetInitVelocityZ();

            double cu = 0.0;
            double usq = velocityX*velocityX + velocityY*velocityY + velocityZ*velocityZ;
            for(int i = 0; i<CELLSIZE; ++i){
                cu = 3.*g_cx[i]*velocityX + g_cy[i]*velocityY + g_cz[i]*velocityZ;
                comp[i] = g_weight[i]*density*(1. + cu + 0.5*cu*cu - (3./2.)*usq);
            }		
    	}  
    
        template<typename COORD_MAP>
        void update( const COORD_MAP& neighborhood, const unsigned& nanoStep );
   
        template<typename COORD_MAP>
        void StreamCollide(const COORD_MAP& neighborhood );
	
        template<typename COORD_MAP>
        void StreamCollide2(const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void CalcMacroscopicValues( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void GridExchange( const COORD_MAP& neighborhood );
	
        template<typename COORD_MAP>
        void CalcDragForce(const COORD_MAP& neighborhood);
	
	// ===== Bounce Back Boundaries =====

        template<typename COORD_MAP>
        void BounceBackBoundaries( const COORD_MAP& neighborhood );
	
        template<typename COORD_MAP>
        void BounceBackBoundaries2( const COORD_MAP& neighborhood );

	// ===== Velocity Boundaries =====
	
        template<typename COORD_MAP>
        void ZouHeWestVelocity( const COORD_MAP& neighborhood);

        template<typename COORD_MAP>
        void ZouHeEastVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void ZouHeNorthVelocity( const COORD_MAP& neighborhood);

        template<typename COORD_MAP>
        void ZouHeSouthVelocity( const COORD_MAP& neighborhood );
	
        template<typename COORD_MAP>
        void ZouHeTopVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void ZouHeBottomVelocity( const COORD_MAP& neighborhood );

	// ===== Pressure Boundaries =====
	
        template<typename COORD_MAP>
        void ZouHeWestPressure( const COORD_MAP& neighborhood );
	
        template<typename COORD_MAP>
        void ZouHeEastPressure( const COORD_MAP& neighborhood );
	
        template<typename COORD_MAP>
        void ZouHeNorthPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP> 
        void ZouHeSouthPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void ZouHeTopPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void ZouHeBottomPressure( const COORD_MAP& neighborhood );

        // ===== Zou He Edges =====
        template<typename COORD_MAP>
        void EdgeBottomWestVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeBottomEastVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeBottomSouthVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeBottomNorthVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeTopWestVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeTopEastVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeTopSouthVelocity( const COORD_MAP& neighborhood );
       
        template<typename COORD_MAP>
        void EdgeTopNorthVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeSouthWestVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeSouthEastVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeNorthWestVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeNorthEastVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeBottomWestPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeBottomEastPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeBottomSouthPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeBottomNorthPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeTopWestPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeTopEastPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeTopSouthPressure( const COORD_MAP& neighborhood );
       
        template<typename COORD_MAP>
        void EdgeTopNorthPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeSouthWestPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeSouthEastPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeNorthWestPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void EdgeNorthEastPressure( const COORD_MAP& neighborhood );

        // ===== Zou He Corners =====

        template<typename COORD_MAP>
        void CornerBottomSouthWestVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void CornerBottomSouthEastVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void CornerBottomNorthWestVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void CornerBottomNorthEastVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void CornerTopSouthWestVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void CornerTopSouthEastVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void CornerTopNorthWestVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void CornerTopNorthEastVelocity( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void CornerBottomSouthWestPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void CornerBottomSouthEastPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void CornerBottomNorthWestPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void CornerBottomNorthEastPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void CornerTopSouthWestPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void CornerTopSouthEastPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void CornerTopNorthWestPressure( const COORD_MAP& neighborhood );

        template<typename COORD_MAP>
        void CornerTopNorthEastPressure( const COORD_MAP& neighborhood );

	// ===== Member Variables =====
        double comp[CELLSIZE];
        double density;
        double velocityX;
        double velocityY;
        double velocityZ;
        State state;
        int bounceback;
        double dragX;
        double dragY;
        double dragZ;
        static Provider provider;
};

Provider Cell::provider;

template<typename COORD_MAP>
void Cell::update(  const COORD_MAP& neighborhood, const unsigned& nanoStep  )
{
  
    GridExchange( neighborhood );
    if (nanoStep == 0)
    {
        dragX = 0.0;
        dragY = 0.0;
        dragZ = 0.0;

        StreamCollide( neighborhood );

        if( (bounceback == 1 ) ){
            BounceBackBoundaries( neighborhood );
        }

        switch( state ){
            case ZOU_HE_BOTTOM_VELOCITY:
                ZouHeBottomVelocity( neighborhood );
                break;
            case ZOU_HE_TOP_VELOCITY:
                ZouHeTopVelocity( neighborhood );
                break;
            case ZOU_HE_NORTH_VELOCITY:
                ZouHeNorthVelocity( neighborhood );
                break;
            case ZOU_HE_SOUTH_VELOCITY:
                ZouHeSouthVelocity( neighborhood );
                break;
            case ZOU_HE_EAST_VELOCITY:
                ZouHeEastVelocity( neighborhood );
                break;
            case ZOU_HE_WEST_VELOCITY:
                ZouHeWestVelocity( neighborhood );
                break;
            case ZOU_HE_BOTTOM_PRESSURE:
                ZouHeBottomPressure( neighborhood );
                break;
            case ZOU_HE_TOP_PRESSURE:
                ZouHeTopPressure( neighborhood );
                break;
            case ZOU_HE_NORTH_PRESSURE:
                ZouHeNorthPressure( neighborhood );
                break;
            case ZOU_HE_SOUTH_PRESSURE:
                ZouHeSouthPressure( neighborhood );
                break;
            case ZOU_HE_EAST_PRESSURE:
                ZouHeEastPressure( neighborhood );
                break;
            case ZOU_HE_WEST_PRESSURE:
                ZouHeWestPressure( neighborhood );
                break;
            case EDGE_TOP_WEST_VELOCITY:
                EdgeTopWestVelocity( neighborhood );
                break;
            case EDGE_TOP_EAST_VELOCITY:
                EdgeTopEastVelocity( neighborhood );
                break;
            case EDGE_TOP_SOUTH_VELOCITY:
                EdgeTopSouthVelocity( neighborhood );
                break;
            case EDGE_TOP_NORTH_VELOCITY:
                EdgeTopNorthVelocity( neighborhood );
                break;
            case EDGE_BOTTOM_WEST_VELOCITY:
                EdgeBottomWestVelocity( neighborhood );
                break;
            case EDGE_BOTTOM_EAST_VELOCITY:
                EdgeBottomEastVelocity( neighborhood );
                break;
            case EDGE_BOTTOM_SOUTH_VELOCITY:
                EdgeBottomSouthVelocity( neighborhood );
                break;
            case EDGE_BOTTOM_NORTH_VELOCITY:
                EdgeBottomNorthVelocity( neighborhood );
                break;
            case EDGE_SOUTH_WEST_VELOCITY:
                EdgeSouthWestVelocity( neighborhood );
                break;
            case EDGE_SOUTH_EAST_VELOCITY:
                EdgeSouthEastVelocity( neighborhood );
                break;
            case EDGE_NORTH_WEST_VELOCITY:
                EdgeNorthWestVelocity( neighborhood );
                break;
            case EDGE_NORTH_EAST_VELOCITY:
                EdgeNorthEastVelocity( neighborhood );
                break;
            case EDGE_TOP_WEST_PRESSURE:
                EdgeTopWestPressure( neighborhood );
                break;
            case EDGE_TOP_EAST_PRESSURE:
                EdgeTopEastPressure( neighborhood );
                break;
            case EDGE_TOP_SOUTH_PRESSURE:
                EdgeTopSouthPressure( neighborhood );
                break;
            case EDGE_TOP_NORTH_PRESSURE:
                EdgeTopNorthPressure( neighborhood );
                break;
            case EDGE_BOTTOM_WEST_PRESSURE:
                EdgeBottomWestPressure( neighborhood );
                break;
            case EDGE_BOTTOM_EAST_PRESSURE:
                EdgeBottomEastPressure( neighborhood );
                break;
            case EDGE_BOTTOM_SOUTH_PRESSURE:
                EdgeBottomSouthPressure( neighborhood );
                break;
            case EDGE_BOTTOM_NORTH_PRESSURE:
                EdgeBottomNorthPressure( neighborhood );
                break;
            case EDGE_SOUTH_WEST_PRESSURE:
                EdgeSouthWestPressure( neighborhood );
                break;
            case EDGE_SOUTH_EAST_PRESSURE:
                EdgeSouthEastPressure( neighborhood );
                break;
            case EDGE_NORTH_WEST_PRESSURE:
                EdgeNorthWestPressure( neighborhood );
                break;
            case EDGE_NORTH_EAST_PRESSURE:
                EdgeNorthEastPressure( neighborhood );
                break;
            case CORNER_BOTTOM_SOUTH_WEST_VELOCITY:
                CornerBottomSouthWestVelocity( neighborhood );
                break;
            case CORNER_BOTTOM_SOUTH_EAST_VELOCITY:
                CornerBottomSouthEastVelocity( neighborhood );
                break;
            case CORNER_BOTTOM_NORTH_WEST_VELOCITY:
                CornerBottomNorthWestVelocity( neighborhood );
                break;
            case CORNER_BOTTOM_NORTH_EAST_VELOCITY:
                CornerBottomNorthEastVelocity( neighborhood );
                break;
            case CORNER_TOP_SOUTH_WEST_VELOCITY:
                CornerTopSouthWestVelocity( neighborhood );
                break;
            case CORNER_TOP_SOUTH_EAST_VELOCITY:
                CornerTopSouthEastVelocity( neighborhood );
                break;
            case CORNER_TOP_NORTH_WEST_VELOCITY:
                CornerTopNorthWestVelocity( neighborhood );
                break;
            case CORNER_TOP_NORTH_EAST_VELOCITY:
                CornerTopNorthEastVelocity( neighborhood );
                break;
            case CORNER_BOTTOM_SOUTH_WEST_PRESSURE:
                CornerBottomSouthWestPressure( neighborhood );
                break;
            case CORNER_BOTTOM_SOUTH_EAST_PRESSURE:
                CornerBottomSouthEastPressure( neighborhood );
                break;
             case CORNER_BOTTOM_NORTH_WEST_PRESSURE:
                CornerBottomNorthWestPressure( neighborhood );
                break;
            case CORNER_BOTTOM_NORTH_EAST_PRESSURE:
                CornerBottomNorthEastPressure( neighborhood );
                break;
            case CORNER_TOP_SOUTH_WEST_PRESSURE:
                CornerTopSouthWestPressure( neighborhood );
                break;
            case CORNER_TOP_SOUTH_EAST_PRESSURE:
                CornerTopSouthEastPressure( neighborhood );
                break;
            case CORNER_TOP_NORTH_WEST_PRESSURE:
                CornerTopNorthWestPressure( neighborhood );
                break;
            case CORNER_TOP_NORTH_EAST_PRESSURE:
                CornerTopNorthEastPressure( neighborhood );
                break;
            default:
                break;
        }

        if(state == OBSTACLE){
             CalcDragForce(neighborhood);
        }

        CalcMacroscopicValues( neighborhood );

    }
}

template<typename COORD_MAP>
void Cell::StreamCollide( const COORD_MAP& neighborhood )
{
	
	//==========================================================
	double tau = provider.GetTau();
	double gx  = provider.GetGravityX()*tau;
	double gy  = provider.GetGravityY()*tau;
	double gz  = provider.GetGravityZ()*tau;
	//==========================================================
	double forceX = provider.GetForceX();
	double forceY = provider.GetForceY();
	double forceZ = provider.GetForceZ();
	//====================================================

	double usq   = 0.0;
	double cu    = 0.0;
	double rt1   = 1./18.;
	double rt2   = 1./36.;
	double force = 0.0;
	
	const double f1 = 3.0;
	const double f2 = 9./2.;
	const double f3 = 3./2.;
        const double omega = provider.CalcOmega();;
        const double omega_trm = 1.0 - omega;
        
	//===== CENTER =====
	double rho  = NEIGH(0,0,0).density;
	double velX = NEIGH(0,0,0).velocityX + gx;
	double velY = NEIGH(0,0,0).velocityY + gy;
	double velZ = NEIGH(0,0,0).velocityZ + gz;

	force = 3.*g_weight[C]*rho*(g_cx[C]*forceX + g_cy[C]*forceY + g_cz[C]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	comp[C] = omega_trm * GET_COMP(0,0,0,C) + omega * ( g_weight[C]*rho * (1. - f3*usq ) ) + force;

	//===== NORTH =====
	rho  = NEIGH(0,-1,0).density;
	velX = NEIGH(0,-1,0).velocityX + gx;
	velY = NEIGH(0,-1,0).velocityY + gy;
	velZ = NEIGH(0,-1,0).velocityZ + gz;

	force = 3.*g_weight[N]*rho*(g_cx[N]*forceX + g_cy[N]*forceY + g_cz[N]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = velY;
	comp[N] =  omega_trm * GET_COMP(0,-1,0,N) + omega * ( rt1*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== SOUTH =====
	rho  = NEIGH(0,1,0).density;
	velX = NEIGH(0,1,0).velocityX + gx;
	velY = NEIGH(0,1,0).velocityY + gy;
	velZ = NEIGH(0,1,0).velocityZ + gz;

	force = 3.*g_weight[S]*rho*(g_cx[S]*forceX + g_cy[S]*forceY + g_cz[S]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = (-velY);
	comp[S] =  omega_trm * GET_COMP(0,1,0,S) + omega * ( rt1*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== WEST =====
	rho  = NEIGH(1,0,0).density;
	velX = NEIGH(1,0,0).velocityX + gx;
	velY = NEIGH(1,0,0).velocityY + gy;
	velZ = NEIGH(1,0,0).velocityZ + gz;

	force = 3.*g_weight[W]*rho*(g_cx[W]*forceX + g_cy[W]*forceY + g_cz[W]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = (-velX);
	comp[W] =  omega_trm * GET_COMP(1,0,0,W) + omega * ( rt1*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== EAST =====
	rho  = NEIGH(-1,0,0).density;
	velX = NEIGH(-1,0,0).velocityX + gx;
	velY = NEIGH(-1,0,0).velocityY + gy;
	velZ = NEIGH(-1,0,0).velocityZ + gz;

	force = 3.*g_weight[E]*rho*(g_cx[E]*forceX + g_cy[E]*forceY + g_cz[E]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = velX;
	comp[E] =  omega_trm * GET_COMP(-1,0,0,E) + omega * ( rt1*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== TOP =====
	rho  = NEIGH(0,0,-1).density;
	velX = NEIGH(0,0,-1).velocityX + gx;
	velY = NEIGH(0,0,-1).velocityY + gy;
	velZ = NEIGH(0,0,-1).velocityZ + gz;

	force = 3.*g_weight[T]*rho*(g_cx[T]*forceX + g_cy[T]*forceY + g_cz[T]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = velZ;
	comp[T] =  omega_trm * GET_COMP(0,0,-1,T) + omega * ( rt1*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== BOTTOM =====
	rho  = NEIGH(0,0,1).density;
	velX = NEIGH(0,0,1).velocityX + gx;
	velY = NEIGH(0,0,1).velocityY + gy;
	velZ = NEIGH(0,0,1).velocityZ + gz;

	force = 3.*g_weight[B]*rho*(g_cx[B]*forceX + g_cy[B]*forceY + g_cz[B]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = (-velZ);
	comp[B] =  omega_trm * GET_COMP(0,0,1,B) + omega * ( rt1*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== NORTH WEST =====
	rho  = NEIGH(1,-1,0).density;
	velX = NEIGH(1,-1,0).velocityX + gx;
	velY = NEIGH(1,-1,0).velocityY + gy;
	velZ = NEIGH(1,-1,0).velocityZ + gz;

	force = 3.*g_weight[NW]*rho*(g_cx[NW]*forceX + g_cy[NW]*forceY + g_cz[NW]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = (-velX+velY);
	comp[NW] =  omega_trm * GET_COMP(1,-1,0,NW) + omega * ( rt2*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== NORTH EAST =====
	rho  = NEIGH(-1,-1,0).density;
	velX = NEIGH(-1,-1,0).velocityX + gx;
	velY = NEIGH(-1,-1,0).velocityY + gy;
	velZ = NEIGH(-1,-1,0).velocityZ + gz;

	force = 3.*g_weight[NE]*rho*(g_cx[NE]*forceX + g_cy[NE]*forceY + g_cz[NE]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = (velX+velY);
	comp[NE] =  omega_trm * GET_COMP(-1,-1,0,NE) + omega * ( rt2*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== SOUTH WEST =====
	rho  = NEIGH(1,1,0).density;
	velX = NEIGH(1,1,0).velocityX + gx;
	velY = NEIGH(1,1,0).velocityY + gy;
	velZ = NEIGH(1,1,0).velocityZ + gz;

	force = 3.*g_weight[SW]*rho*(g_cx[SW]*forceX + g_cy[SW]*forceY + g_cz[SW]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = (-velX-velY);
	comp[SW] =  omega_trm * GET_COMP(1,1,0,SW) + omega * ( rt2*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== SOUTH EAST =====
	rho  = NEIGH(-1,1,0).density;
	velX = NEIGH(-1,1,0).velocityX + gx;
	velY = NEIGH(-1,1,0).velocityY + gy;
	velZ = NEIGH(-1,1,0).velocityZ + gz;


	force = 3.*g_weight[SE]*rho*(g_cx[SE]*forceX + g_cy[SE]*forceY + g_cz[SE]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = (velX-velY);
	comp[SE] =  omega_trm * GET_COMP(-1,1,0,SE) + omega * ( rt2*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== TOP NORTH =====
	rho  = NEIGH(0,-1,-1).density;
	velX = NEIGH(0,-1,-1).velocityX + gx;
	velY = NEIGH(0,-1,-1).velocityY + gy;
	velZ = NEIGH(0,-1,-1).velocityZ + gz;

	force = 3.*g_weight[TN]*rho*(g_cx[TN]*forceX + g_cy[TN]*forceY + g_cz[TN]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = (velY+velZ);
	comp[TN] =  omega_trm * GET_COMP(0,-1,-1,TN) + omega * ( rt2*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== TOP SOUTH =====
	rho  = NEIGH(0,1,-1).density;
	velX = NEIGH(0,1,-1).velocityX + gx;
	velY = NEIGH(0,1,-1).velocityY + gy;
	velZ = NEIGH(0,1,-1).velocityZ + gz;

	force = 3.*g_weight[TS]*rho*(g_cx[TS]*forceX + g_cy[TS]*forceY + g_cz[TS]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = (-velY+velZ);
	comp[TS] =  omega_trm * GET_COMP(0,1,-1,TS) + omega * ( rt2*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== TOP WEST =====
	rho  = NEIGH(1,0,-1).density;
	velX = NEIGH(1,0,-1).velocityX + gx;
	velY = NEIGH(1,0,-1).velocityY + gy;
	velZ = NEIGH(1,0,-1).velocityZ + gz;

	force = 3.*g_weight[TW]*rho*(g_cx[TW]*forceX + g_cy[TW]*forceY + g_cz[TW]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = (-velX+velZ);
	comp[TW] =  omega_trm * GET_COMP(1,0,-1,TW) + omega * ( rt2*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== TOP EAST =====
	rho  = NEIGH(-1,0,-1).density;
	velX = NEIGH(-1,0,-1).velocityX + gx;
	velY = NEIGH(-1,0,-1).velocityY + gy;
	velZ = NEIGH(-1,0,-1).velocityZ + gz;

	force = 3.*g_weight[TE]*rho*(g_cx[TE]*forceX + g_cy[TE]*forceY + g_cz[TE]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = (velX+velZ);
	comp[TE] =  omega_trm * GET_COMP(-1,0,-1,TE) + omega * ( rt2*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== BOTTOM NORTH =====
	rho  = NEIGH(0,-1,1).density;
	velX = NEIGH(0,-1,1).velocityX + gx;
	velY = NEIGH(0,-1,1).velocityY + gy;
	velZ = NEIGH(0,-1,1).velocityZ + gz;

	force = 3.*g_weight[BN]*rho*(g_cx[BN]*forceX + g_cy[BN]*forceY + g_cz[BN]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = (velY-velZ);
	comp[BN] =  omega_trm * GET_COMP(0,-1,1,BN) + omega * ( rt2*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== BOTTOM SOUTH =====
	rho  = NEIGH(0,1,1).density;
	velX = NEIGH(0,1,1).velocityX + gx;
	velY = NEIGH(0,1,1).velocityY + gy;
	velZ = NEIGH(0,1,1).velocityZ + gz;

	force = 3.*g_weight[BS]*rho*(g_cx[BS]*forceX + g_cy[BS]*forceY + g_cz[BS]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = (-velY-velZ);
	comp[BS] =  omega_trm * GET_COMP(0,1,1,BS) + omega * ( rt2*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== BOTTOM WEST =====
	rho  = NEIGH(1,0,1).density;
	velX = NEIGH(1,0,1).velocityX + gx;
	velY = NEIGH(1,0,1).velocityY + gy;
	velZ = NEIGH(1,0,1).velocityZ + gz;

	force = 3.*g_weight[BW]*rho*(g_cx[BW]*forceX + g_cy[BW]*forceY + g_cz[BW]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = (-velX-velZ);
	comp[BW] =  omega_trm * GET_COMP(1,0,1,BW) + omega * ( rt2*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

	//===== BOTTOM EAST =====
	rho  = NEIGH(-1,0,1).density;
	velX = NEIGH(-1,0,1).velocityX + gx;
	velY = NEIGH(-1,0,1).velocityY + gy;
	velZ = NEIGH(-1,0,1).velocityZ + gz;

	force = 3.*g_weight[BE]*rho*(g_cx[BE]*forceX + g_cy[BE]*forceY + g_cz[BE]*forceZ);
	usq = (velX*velX + velY*velY + velZ*velZ);
	cu = (velX-velZ);
	comp[BE] =  omega_trm * GET_COMP(-1,0,1,BE) + omega * ( rt2*rho * (1. + f1*cu + f2*(cu*cu) - f3*usq) ) + force;

}

template<typename COORD_MAP>
void Cell::BounceBackBoundaries( const COORD_MAP& neighborhood )
{

    int streamX[] = {0,0,0,1,-1,0,0,1,-1,1,-1,0,0,1,-1,0,0,1,-1};
    int streamY[] = {0,-1,1,0,0,0,0,-1,-1,1,1,-1,1,0,0,-1,1,0,0};
    int streamZ[] = {0,0,0,0,0,-1,1,0,0,0,0,-1,-1,-1,-1,1,1,1,1};

    for(int i = 1; i < CELLSIZE; ++i ){
        State tmp_state = GET_STATE(streamX[i],streamY[i],streamZ[i]);
        
        if( tmp_state == NO_SLIP ){
            comp[i] = GET_COMP(streamX[i],streamY[i],streamZ[i],g_stream[i]);
        }
        else if( tmp_state == OBSTACLE ){
            comp[i] = GET_COMP(streamX[i],streamY[i],streamZ[i],g_stream[i]);
        }
        else if( tmp_state == FREE_SLIP_N_S ){
            comp[i] = GET_COMP(0,streamY[i],0,g_freeY[i]);
        }
        else if( tmp_state == FREE_SLIP_E_W ){
            comp[i] = GET_COMP(streamX[i],0,0,g_freeX[i]);
        }
        else if( tmp_state == FREE_SLIP_T_B ){
            comp[i] = GET_COMP(0,0,streamZ[i],g_freeZ[i]);
        }
        else if( tmp_state == ACCELERATION ){
            double tmp_ux = provider.GetTypeVelocityX("ACCELERATION");
            double tmp_uy = provider.GetTypeVelocityY("ACCELERATION");
            double tmp_uz = provider.GetTypeVelocityZ("ACCELERATION");
            comp[i] = GET_COMP(streamX[i], streamY[i], streamZ[i], g_stream[i] ) + 6 * g_weight[i] * ( g_cx[i]*tmp_ux + g_cy[i]*tmp_uy + g_cz[i]*tmp_uz );
        }
        else{
            continue;
        }
    }
}

template<typename COORD_MAP>
void Cell::CalcDragForce( const COORD_MAP& neighborhood )
{
    for( int i = 1; i < CELLSIZE; ++i){
        if(GET_STATE(g_pullX[i], g_pullY[i], g_pullZ[i]) == FLUID){
            dragX = dragX + g_cx[i] * ( 2.0 * comp[i]);
            dragY = dragY + g_cy[i] * ( 2.0 * comp[i]);
            dragZ = dragZ + g_cz[i] * ( 2.0 * comp[i]);
        }
    }
}

template<typename COORD_MAP>
void Cell::CalcMacroscopicValues( const COORD_MAP& neighborhood )
{
    double rho  = 0.0;
    double velX = 0.0;
    double velY = 0.0;
    double velZ = 0.0;
    for( int i = 0; i<CELLSIZE; ++i){
        rho  = rho + comp[i];
        velX = velX + g_cx[i]*comp[i];
        velY = velY + g_cy[i]*comp[i];
        velZ = velZ + g_cz[i]*comp[i];
    }
    density = rho;
    velocityX = velX * 1./rho;
    velocityY = velY * 1./rho;
    velocityZ = velZ * 1./rho;
}

template<typename COORD_MAP>
void Cell::GridExchange(const COORD_MAP& neighborhood )
{
    state = GET_STATE(0,0,0);
    bounceback = NEIGH(0,0,0).bounceback;	
	
    velocityX = NEIGH(0,0,0).velocityX;
    velocityY = NEIGH(0,0,0).velocityY;
    velocityZ = NEIGH(0,0,0).velocityZ;
    density   = NEIGH(0,0,0).density; 

    for( int i = 0; i < CELLSIZE; ++i ){
        comp[i] = GET_COMP(0,0,0,i);
    }
}

template<typename COORD_MAP>
void Cell::ZouHeTopVelocity( const COORD_MAP& neighborhood )
{
    double velZ = provider.GetVelocityZ("Top");
    double velX = provider.GetVelocityX("Top");
    double velY = provider.GetVelocityY("Top");

    double rho = 1./(velZ+1.) * ( comp[E] + comp[W] + comp[N] + comp[S] + comp[NE] + comp[NW] + comp[SW] + comp[SE] + comp[C] 
               + 2.*(comp[T] + comp[TE] + comp[TW] + comp[TN] + comp[TS]) );

    double f1 = (1./3.);
    double f2 = (rho/6.);
	
    double Nzx = 0.5 * ( comp[E] + comp[NE] + comp[SE] - (comp[W]+comp[NW]+comp[SW]) ) - (1./3.)*rho*velX;
    double Nzy = 0.5 * ( comp[N] + comp[NE] + comp[NW] - (comp[S]+comp[SE]+comp[SW]) ) - (1./3.)*rho*velY;

    comp[ B] = comp[T ] - f1 * rho*velZ;
    comp[BE] = comp[TW] + f2 * (-velZ+velX) - Nzx;
    comp[BW] = comp[TE] + f2 * (-velZ-velX) + Nzx;
    comp[BN] = comp[TS] + f2 * (-velZ+velY) - Nzy;
    comp[BS] = comp[TN] + f2 * (-velZ-velY) + Nzy; 
}

template<typename COORD_MAP>
void Cell::ZouHeBottomVelocity( const COORD_MAP& neighborhood )
{
    double velZ = provider.GetVelocityZ("Bottom");
    double velX = provider.GetVelocityX("Bottom");
    double velY = provider.GetVelocityY("Bottom");

    double rho = 1./(1.-velZ) * ( comp[E] + comp[W] + comp[N] + comp[S] + comp[NE] + comp[SE] + comp[NW] + comp[SW] + comp[C] 
               + 2.*(comp[B] + comp[BE] + comp[BW] + comp[BN] + comp[BS]) );
	
    double f1 = 1./3.;
    double f2 = rho/6.;

    double Nzx = 0.5 * ( comp[E] + comp[NE] + comp[SE] - (comp[W]+comp[NW]+comp[SW]) ) - (1./3.)*rho*velX;
    double Nzy = 0.5 * ( comp[N] + comp[NE] + comp[NW] - (comp[S]+comp[SE]+comp[SW]) ) - (1./3.)*rho*velY;

    comp[ T] = comp[B ] + f1 * rho*velZ;
    comp[TE] = comp[BW] + f2 * (velZ+velX) - Nzx;
    comp[TW] = comp[BE] + f2 * (velZ-velX) + Nzx;
    comp[TN] = comp[BS] + f2 * (velZ+velY) - Nzy;
    comp[TS] = comp[BN] + f2 * (velZ-velY) + Nzy;

}

template<typename COORD_MAP>
void Cell::ZouHeNorthVelocity( const COORD_MAP& neighborhood )
{
    double velY = provider.GetVelocityY("North");
    double velX = provider.GetVelocityX("North");
    double velZ = provider.GetVelocityZ("North");

    double rho = 1./(velY+1.) * ( comp[E] + comp[W] + comp[T] + comp[B] + comp[TE] + comp[BE] + comp[TW] + comp[BW] + comp[C] 
               + 2.*(comp[N] + comp[NE] + comp[NW] + comp[TN] + comp[BN]) );
	
    double f1 = 1./3.;
    double f2 = rho/6.;

    double Nyx = 0.5 * ( comp[E] + comp[TE] + comp[BE] - (comp[W]+comp[TW]+comp[BW]) ) - (1./3.)*rho*velX;
    double Nyz = 0.5 * ( comp[T] + comp[TE] + comp[TW] - (comp[B]+comp[BE]+comp[BW]) ) - (1./3.)*rho*velZ;

    comp[ S] = comp[ N] - f1 * rho*velY;
    comp[SW] = comp[NE] + f2 * (-velY-velX) + Nyx;
    comp[SE] = comp[NW] + f2 * (-velY+velX) - Nyx;
    comp[BS] = comp[TN] + f2 * (-velY-velZ) + Nyz;
    comp[TS] = comp[BN] + f2 * (-velY+velZ) - Nyz;

}

template<typename COORD_MAP>
void Cell::ZouHeSouthVelocity( const COORD_MAP& neighborhood )
{
    double velY = provider.GetVelocityY("South");
    double velX = provider.GetVelocityX("South");
    double velZ = provider.GetVelocityZ("South");

    double rho = 1./(1.-velY) * ( comp[E] + comp[W] + comp[T] + comp[B] + comp[TE] + comp[BE] + comp[TW] + comp[BW] + comp[C] 
               + 2.*(comp[S] + comp[SE] + comp[SW] + comp[TS] + comp[BS]) );
	
    double f1 = 1./3.;
    double f2 = rho/6.;

    double Nyx = 0.5 * ( comp[E] + comp[TE] + comp[BE] - (comp[W]+comp[TW]+comp[BW]) ) - (1./3.)*rho*velX;
    double Nyz = 0.5 * ( comp[T] + comp[TE] + comp[TW] - (comp[B]+comp[BE]+comp[BW]) ) - (1./3.)*rho*velZ;

    comp[ N] = comp[ S] + f1 * rho*velY;
    comp[NE] = comp[SW] + f2 * (velY+velX) - Nyx;
    comp[NW] = comp[SE] + f2 * (velY-velX) + Nyx;
    comp[TN] = comp[BS] + f2 * (velY+velZ) - Nyz;
    comp[BN] = comp[TS] + f2 * (velY-velZ) + Nyz;

}
template<typename COORD_MAP>
void Cell::ZouHeWestVelocity( const COORD_MAP& neighborhood )
{
    double velX = provider.GetVelocityX("West");
    double velY = provider.GetVelocityY("West");
    double velZ = provider.GetVelocityZ("West");

    double rho = 1./(1.-velX) * ( comp[N] + comp[S] + comp[T] + comp[B] + comp[TN] + comp[BN] + comp[TS] + comp[BS] + comp[C] 
               + 2.*(comp[W] + comp[NW] + comp[SW] + comp[TW] + comp[BW]) );
	
    double f1 = 1./3.;
    double f2 = rho/6.;

    double Nxy = 0.5 * ( comp[N] + comp[TN] + comp[BN] - (comp[S]+comp[TS]+comp[BS]) ) - f1*rho*velY;
    double Nxz = 0.5 * ( comp[T] + comp[TS] + comp[TN] - (comp[B]+comp[BN]+comp[BS]) ) - f1*rho*velZ;

    comp[ E] = comp[W ] + f1 * rho*velX;
    comp[SE] = comp[NW] + f2 * (velX-velY) + Nxy;
    comp[NE] = comp[SW] + f2 * (velX+velY) - Nxy;
    comp[TE] = comp[BW] + f2 * (velX+velZ) - Nxz;
    comp[BE] = comp[TW] + f2 * (velX-velZ) + Nxz;
    
}

template<typename COORD_MAP>
void Cell::ZouHeEastVelocity( const COORD_MAP& neighborhood )
{
    double velX = provider.GetVelocityX("East");
    double velY = provider.GetVelocityY("East");
    double velZ = provider.GetVelocityZ("East");

    double rho = (1./(velX+1.)) * ( comp[N] + comp[S] + comp[T] + comp[B] + comp[TN] + comp[BN] + comp[TS] + comp[BS] + comp[C] 
               + 2.*(comp[E] + comp[NE] + comp[SE] + comp[TE] + comp[BE]) );
	
    double f1 = 1./3.;
    double f2 = rho/6.;

    double Nxy = 0.5 * ( comp[N] + comp[TN] + comp[BN] - (comp[S]+comp[TS]+comp[BS]) ) - f1*rho*velY;
    double Nxz = 0.5 * ( comp[T] + comp[TS] + comp[TN] - (comp[B]+comp[BN]+comp[BS]) ) - f1*rho*velZ;

    comp[ W] = comp[E ] - f1 * rho*velX;
    comp[NW] = comp[SE] + f2 * (-velX+velY) - Nxy;
    comp[SW] = comp[NE] + f2 * (-velX-velY) + Nxy;
    comp[BW] = comp[TE] + f2 * (-velX-velZ) + Nxz;
    comp[TW] = comp[BE] + f2 * (-velX+velZ) - Nxz;
}

template<typename COORD_MAP>
void Cell::ZouHeTopPressure( const COORD_MAP& neighborhood )
{
    double rho  = provider.GetPressure("Top");
    double velX = provider.GetVelocityX("Top");
    double velY = provider.GetVelocityY("Top");	

    double velZ = -1. + (1./rho) * (comp[E] + comp[W] + comp[N] + comp[S] + comp[NE] + comp[NW] + comp[SW] + comp[SE] + comp[C]
                + 2.*(comp[T] + comp[TE] + comp[TW] + comp[TN] + comp[TS]) ); 

    double f1 = (1./3.);
    double f2 = (rho/6.);

    double Nzx = 0.5 * ( comp[E] + comp[NE] + comp[SE] - (comp[W]+comp[NW]+comp[SW]) ) - (1./3.)*rho*velX;
    double Nzy = 0.5 * ( comp[N] + comp[NE] + comp[NW] - (comp[S]+comp[SE]+comp[SW]) ) - (1./3.)*rho*velY;

    comp[ B] = comp[T ] - f1 * rho*velZ;
    comp[BE] = comp[TW] + f2 * (-velZ+velX) - Nzx;
    comp[BW] = comp[TE] + f2 * (-velZ-velX) + Nzx;
    comp[BN] = comp[TS] + f2 * (-velZ+velY) - Nzy;
    comp[BS] = comp[TN] + f2 * (-velZ-velY) + Nzy;  

}

template<typename COORD_MAP>
void Cell::ZouHeBottomPressure( const COORD_MAP& neighborhood )
{
    double rho  = provider.GetPressure("Bottom");
    double velX = provider.GetVelocityX("Bottom");
    double velY = provider.GetVelocityY("Bottom");

    double velZ = 1. - (1./rho) * ( comp[E] + comp[W] + comp[N] + comp[S] + comp[NE] + comp[SE] + comp[NW] + comp[SW] + comp[C] 
                + 2.*(comp[B] + comp[BE] + comp[BW] + comp[BN] + comp[BS]) );
	
    double f1 = 1./3.;
    double f2 = rho/6.;

    double Nzx = 0.5 * ( comp[E] + comp[NE] + comp[SE] - (comp[W]+comp[NW]+comp[SW]) ) - (1./3.)*rho*velX;
    double Nzy = 0.5 * ( comp[N] + comp[NE] + comp[NW] - (comp[S]+comp[SE]+comp[SW]) ) - (1./3.)*rho*velY;

    comp[ T] = comp[B ] + f1 * rho*velZ;
    comp[TW] = comp[BE] + f2 * (velZ-velX) + Nzx;
    comp[TE] = comp[BW] + f2 * (velZ+velX) - Nzx;
    comp[TS] = comp[BN] + f2 * (velZ-velY) + Nzy;
    comp[TN] = comp[BS] + f2 * (velZ+velY) - Nzy;
}

template<typename COORD_MAP>
void Cell::ZouHeWestPressure( const COORD_MAP& neighborhood )
{
    double rho  = provider.GetPressure("West");
    double velY = provider.GetVelocityY("West");
    double velZ = provider.GetVelocityZ("West");

    double velX = 1. - (1./rho) * ( comp[N] + comp[S] + comp[T] + comp[B] + comp[TN] + comp[BN] + comp[TS] + comp[BS] + comp[C] 
                + 2.*(comp[W] + comp[NW] + comp[SW] + comp[TW] + comp[BW]) );
	
    double f1 = 1./3.;
    double f2 = rho/6.;

    double Nxy = 0.5 * ( comp[N] + comp[TN] + comp[BN] - (comp[S]+comp[TS]+comp[BS]) ) - (1./3.)*rho*velY;
    double Nxz = 0.5 * ( comp[T] + comp[TS] + comp[TN] - (comp[B]+comp[BN]+comp[BS]) ) - (1./3.)*rho*velZ;

    comp[ E] = comp[ W] + f1 * rho*velX;
    comp[SE] = comp[NW] + f2 * (velX-velY) + Nxy;
    comp[NE] = comp[SW] + f2 * (velX+velY) - Nxy;
    comp[TE] = comp[BW] + f2 * (velX+velZ) - Nxz;
    comp[BE] = comp[TW] + f2 * (velX-velZ) + Nxz;
}

template<typename COORD_MAP>
void Cell::ZouHeEastPressure( const COORD_MAP& neighborhood )
{
    double rho  = provider.GetPressure("East");
    double velY = provider.GetVelocityY("East");
    double velZ = provider.GetVelocityZ("East");

    double velX = -1. + (1./rho) * ( comp[N] + comp[S] + comp[T] + comp[B] + comp[TN] + comp[BN] + comp[TS] + comp[BS] + comp[C] 
                + 2.*(comp[E] + comp[NE] + comp[SE] + comp[TE] + comp[BE]) );
	
    double f1 = 1./3.;
    double f2 = rho/6.;

    double Nxy = 0.5 * ( comp[N] + comp[TN] + comp[BN] - (comp[S]+comp[TS]+comp[BS]) ) - (1./3.)*rho*velY;
    double Nxz = 0.5 * ( comp[T] + comp[TS] + comp[TN] - (comp[B]+comp[BN]+comp[BS]) ) - (1./3.)*rho*velZ;

    comp[ W] = comp[ E] - f1 * rho*velX;
    comp[NW] = comp[SE] + f2 * (-velX+velY) - Nxy;
    comp[SW] = comp[NE] + f2 * (-velX-velY) + Nxy;
    comp[BW] = comp[TE] + f2 * (-velX-velZ) + Nxz;
    comp[TW] = comp[BE] + f2 * (-velX+velZ) - Nxz;
}

template<typename COORD_MAP>
void Cell::ZouHeNorthPressure( const COORD_MAP& neighborhood )
{
    double rho = provider.GetPressure("North"); 
    double velX = provider.GetVelocityX("North");
    double velZ = provider.GetVelocityZ("North");

    double velY = -1. + 1./rho * ( comp[E] + comp[W] + comp[T] + comp[B] + comp[TE] + comp[BE] + comp[TW] + comp[BW] + comp[C] 
               + 2.*(comp[N] + comp[NE] + comp[NW] + comp[TN] + comp[BN]) );
	
    double f1 = 1./3.;
    double f2 = rho/6.;

    double Nyx = 0.5 * ( comp[E] + comp[TE] + comp[BE] - (comp[W]+comp[TW]+comp[BW]) ) - (1./3.)*rho*velX;
    double Nyz = 0.5 * ( comp[T] + comp[TE] + comp[TW] - (comp[B]+comp[BE]+comp[BW]) ) - (1./3.)*rho*velZ;

    comp[ S] = comp[ N] - f1 * rho*velY;
    comp[SW] = comp[NE] + f2 * (-velY-velX) + Nyx;
    comp[SE] = comp[NW] + f2 * (-velY+velX) - Nyx;
    comp[BS] = comp[TN] + f2 * (-velY-velZ) + Nyz;
    comp[TS] = comp[BN] + f2 * (-velY+velZ) - Nyz;

}

template<typename COORD_MAP>
void Cell::ZouHeSouthPressure( const COORD_MAP& neighborhood )
{
    double rho  = provider.GetPressure("South");
    double velX = provider.GetVelocityX("South");
    double velZ = provider.GetVelocityZ("South");

    double velY = 1. - 1./rho  * ( comp[E] + comp[W] + comp[T] + comp[B] + comp[TE] + comp[BE] + comp[TW] + comp[BW] + comp[C] 
               + 2.*(comp[S] + comp[SE] + comp[SW] + comp[TS] + comp[BS]) );
	
    double f1 = 1./3.;
    double f2 = rho/6.;

    double Nyx = 0.5 * ( comp[E] + comp[TE] + comp[BE] - (comp[W]+comp[TW]+comp[BW]) ) - (1./3.)*rho*velX;
    double Nyz = 0.5 * ( comp[T] + comp[TE] + comp[TW] - (comp[B]+comp[BE]+comp[BW]) ) - (1./3.)*rho*velZ;

    comp[ N] = comp[ S] + f1 * rho*velY;
    comp[NE] = comp[SW] + f2 * (velY+velX) - Nyx;
    comp[NW] = comp[SE] + f2 * (velY-velX) + Nyx;
    comp[TN] = comp[BS] + f2 * (velY+velZ) - Nyz;
    comp[BN] = comp[TS] + f2 * (velY-velZ) + Nyz;
}

template<typename COORD_MAP>
void Cell::EdgeBottomWestVelocity( const COORD_MAP& neighborhood )
{
    double corr = (1./4.) * (comp[N]-comp[S]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[E ] = comp[W ];
    comp[T ] = comp[B ];
    comp[TE] = comp[BW];

    comp[SE] = comp[NW] + corr;
    comp[NE] = comp[SW] - corr;
    comp[TS] = comp[BN] + corr;
    comp[TN] = comp[BS] - corr;

   double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[NE] + comp[SW] + comp[SE] + comp[TN] + comp[TS] 
                     + comp[TE] + comp[BN] + comp[BS] + comp[BW] );

    comp[BE] = erg;
    comp[TW] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeBottomEastVelocity( const COORD_MAP& neighborhood )
{
    double corr = (1./4.) * (comp[N]-comp[S]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[W ] = comp[E ];
    comp[T ] = comp[B ];
    comp[TW] = comp[BE];

    comp[NW] = comp[SE] - corr;
    comp[SW] = comp[NE] + corr;
    comp[TS] = comp[BN] + corr;
    comp[TN] = comp[BS] - corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[NE] + comp[SW] + comp[SE] + comp[TN] + comp[TS] 
                     + comp[TW] + comp[BN] + comp[BS] + comp[BE] );

    comp[BW] = erg;
    comp[TE] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeBottomSouthVelocity( const COORD_MAP& neighborhood )
{
    double corr = (1./4.) * (comp[E]-comp[W]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[N ] = comp[S ];
    comp[T ] = comp[B ];
    comp[TN] = comp[BS];

    comp[NE] = comp[SW] - corr;
    comp[NW] = comp[SE] + corr;
    comp[TW] = comp[BE] + corr;
    comp[TE] = comp[BW] - corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[NE] + comp[SW] + comp[SE] + comp[TN] + comp[TW] 
                     + comp[TE] + comp[BS] + comp[BW] + comp[BE] );

    comp[TS] = erg;
    comp[BN] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeBottomNorthVelocity( const COORD_MAP& neighborhood )
{
    double corr = (1./4.) * (comp[E]-comp[W]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[S ] = comp[N ];
    comp[T ] = comp[B ];
    comp[TS] = comp[BN];

    comp[SW] = comp[NE] + corr;
    comp[SE] = comp[NW] - corr;
    comp[TW] = comp[BE] + corr;
    comp[TE] = comp[BW] - corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[NE] + comp[SW] + comp[SE] + comp[TS] + comp[TW] 
                     + comp[TE] + comp[BN] + comp[BW] + comp[BE] );

    comp[BS] = erg;
    comp[TN] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeTopSouthVelocity( const COORD_MAP& neighborhood )
{
    double corr = (1./4.) * (comp[E]-comp[W]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[N ] = comp[S ];
    comp[B ] = comp[T ];
    comp[BN] = comp[TS];

    comp[NE] = comp[SW] - corr;
    comp[NW] = comp[SE] + corr;
    comp[BW] = comp[TE] + corr;
    comp[BE] = comp[TW] - corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[NE] + comp[SW] + comp[SE] + comp[TS] + comp[TW] 
                     + comp[TE] + comp[BN] + comp[BW] + comp[BE] );

    comp[BS] = erg;
    comp[TN] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeTopNorthVelocity( const COORD_MAP& neighborhood )
{
    double corr = (1./4.) * (comp[E]-comp[W]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[S ] = comp[N ];
    comp[B ] = comp[T ];
    comp[BS] = comp[TN];

    comp[SW] = comp[NE] + corr;
    comp[SE] = comp[NW] - corr;
    comp[BW] = comp[TE] + corr;
    comp[BE] = comp[TW] - corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[NE] + comp[SW] + comp[SE] + comp[TN] + comp[TW] 
                     + comp[TE] + comp[BS] + comp[BW] + comp[BE] );


    comp[TS] = erg;
    comp[BN] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeTopEastVelocity( const COORD_MAP& neighborhood )
{
    double corr = (1./4.) * (comp[N]-comp[S]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[W ] = comp[E ];
    comp[B ] = comp[T ];
    comp[BW] = comp[TE];

    comp[NW] = comp[SE] - corr;
    comp[SW] = comp[NE] + corr;
    comp[BN] = comp[TS] - corr;
    comp[BS] = comp[TN] + corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[NE] + comp[SW] + comp[SE] + comp[TN] + comp[TS] 
                     + comp[TE] + comp[BN] + comp[BS] + comp[BW] );

    comp[BE] = erg;
    comp[TW] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeTopWestVelocity( const COORD_MAP& neighborhood )
{
    double corr = (1./4.) * (comp[N]-comp[S]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[E ] = comp[W ];
    comp[B ] = comp[T ];
    comp[BE] = comp[TW];

    comp[SE] = comp[NW] + corr;
    comp[NE] = comp[SW] - corr;
    comp[BN] = comp[TS] - corr;
    comp[BS] = comp[TN] + corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ E] + comp[ W] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[NE] + comp[SW] + comp[SE] + comp[TN] + comp[TS] 
                     + comp[TW] + comp[BN] + comp[BS] + comp[BE] );

    comp[TE] = erg;
    comp[BW] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeSouthWestVelocity( const COORD_MAP& neighborhood )
{
    double corr = (1./4.) * (comp[T]-comp[B]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[E ] = comp[W ];
    comp[N ] = comp[S ];
    comp[NE] = comp[SW];

    comp[TE] = comp[BW] - corr;
    comp[BE] = comp[TW] + corr;
    comp[TN] = comp[BS] - corr;
    comp[BN] = comp[TS] + corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NE] + comp[SW] + comp[TN] + comp[TS] + comp[TW] + comp[TE] 
                     + comp[BN] + comp[BS] + comp[BW] + comp[BE] );

    comp[SE] = erg;
    comp[NW] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeSouthEastVelocity( const COORD_MAP& neighborhood )
{
    double corr = (1./4.) * (comp[T]-comp[B]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[W ] = comp[E ];
    comp[N ] = comp[S ];
    comp[NW] = comp[SE];

    comp[BW] = comp[TE] + corr;
    comp[TW] = comp[BE] - corr;
    comp[TN] = comp[BS] - corr;
    comp[BN] = comp[TS] + corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[SE] + comp[TN] + comp[TS] + comp[TW] + comp[TE] 
                     + comp[BN] + comp[BS] + comp[BW] + comp[BE] );

    comp[NE] = erg;
    comp[SW] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeNorthWestVelocity( const COORD_MAP& neighborhood )
{
    double corr = (1./4.) * (comp[T]-comp[B]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[E ] = comp[W ];
    comp[S ] = comp[N ];
    comp[SE] = comp[NW];

    comp[TE] = comp[BW] - corr;
    comp[BE] = comp[TW] + corr;
    comp[BS] = comp[TN] + corr;
    comp[TS] = comp[BN] - corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                   + comp[NW] + comp[SE] + comp[TN] + comp[TS] + comp[TW] + comp[TE] 
                   + comp[BN] + comp[BS] + comp[BW] + comp[BE] );

    comp[NE] = erg;
    comp[SW] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeNorthEastVelocity( const COORD_MAP& neighborhood )
{
    double corr = (1./4.) * (comp[T]-comp[B]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[W ] = comp[E ];
    comp[S ] = comp[N ];
    comp[SW] = comp[NE];

    comp[BW] = comp[TE] + corr;
    comp[TW] = comp[BE] - corr;
    comp[BS] = comp[TN] + corr;
    comp[TS] = comp[BN] - corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NE] + comp[SW] + comp[TN] + comp[TS] + comp[TW] + comp[TE] 
                     + comp[BN] + comp[BS] + comp[BW] + comp[BE] );

    comp[SE] = erg;
    comp[NW] = erg;
    comp[C] = f3*erg;
}
//=========================  PRESSURE  ================================================================
template<typename COORD_MAP>
void Cell::EdgeBottomEastPressure( const COORD_MAP& neighborhood )
{
    double rho = provider.GetPressure("BottomEast");
    double corr = (1./4.) * (comp[N]-comp[S]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[W ] = comp[E ];
    comp[T ] = comp[B ];
    comp[TW] = comp[BE];

    comp[NW] = comp[SE] - corr;
    comp[SW] = comp[NE] + corr;
    comp[TS] = comp[BN] + corr;
    comp[TN] = comp[BS] - corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[NE] + comp[SW] + comp[SE] + comp[TN] + comp[TS] 
                     + comp[TW] + comp[BN] + comp[BS] + comp[BE] );
 
    erg = (rho - 22.*erg)/14.; 
    
    comp[BW] = erg;
    comp[TE] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeBottomWestPressure( const COORD_MAP& neighborhood )
{
    double rho = provider.GetPressure("BottomWest");
    double corr = (1./4.) * (comp[N]-comp[S]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[E ] = comp[W ];
    comp[T ] = comp[B ];
    comp[TE] = comp[BW];

    comp[SE] = comp[NW] + corr;
    comp[NE] = comp[SW] - corr;
    comp[TS] = comp[BN] + corr;
    comp[TN] = comp[BS] - corr;

   double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[NE] + comp[SW] + comp[SE] + comp[TN] + comp[TS] 
                     + comp[TE] + comp[BN] + comp[BS] + comp[BW] );

    erg = (rho - 22.*erg)/14.; 

    comp[BE] = erg;
    comp[TW] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeBottomSouthPressure( const COORD_MAP& neighborhood )
{
    double rho = provider.GetPressure("BottomSouth"); 
    double corr = (1./4.) * (comp[E]-comp[W]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[N ] = comp[S ];
    comp[T ] = comp[B ];
    comp[TN] = comp[BS];

    comp[NE] = comp[SW] - corr;
    comp[NW] = comp[SE] + corr;
    comp[TW] = comp[BE] + corr;
    comp[TE] = comp[BW] - corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[NE] + comp[SW] + comp[SE] + comp[TN] + comp[TW] 
                     + comp[TE] + comp[BS] + comp[BW] + comp[BE] );

    erg = (rho - 22.*erg)/14.; 

    comp[TS] = erg;
    comp[BN] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeBottomNorthPressure( const COORD_MAP& neighborhood )
{
    double rho = provider.GetPressure("BottomNorth"); 
    double corr = (1./4.) * (comp[E]-comp[W]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[S ] = comp[N ];
    comp[T ] = comp[B ];
    comp[TS] = comp[BN];

    comp[SW] = comp[NE] + corr;
    comp[SE] = comp[NW] - corr;
    comp[TW] = comp[BE] + corr;
    comp[TE] = comp[BW] - corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[NE] + comp[SW] + comp[SE] + comp[TS] + comp[TW] 
                     + comp[TE] + comp[BN] + comp[BW] + comp[BE] );

    erg = (rho - 22.*erg)/14.; 

    comp[BS] = erg;
    comp[TN] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeTopSouthPressure( const COORD_MAP& neighborhood )
{
    double rho = provider.GetPressure("TopSouth"); 
    double corr = (1./4.) * (comp[E]-comp[W]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[N ] = comp[S ];
    comp[B ] = comp[T ];
    comp[BN] = comp[TS];

    comp[NE] = comp[SW] - corr;
    comp[NW] = comp[SE] + corr;
    comp[BW] = comp[TE] + corr;
    comp[BE] = comp[TW] - corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[NE] + comp[SW] + comp[SE] + comp[TS] + comp[TW] 
                     + comp[TE] + comp[BN] + comp[BW] + comp[BE] );

    erg = (rho - 22.*erg)/14.; 

    comp[BS] = erg;
    comp[TN] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeTopNorthPressure( const COORD_MAP& neighborhood )
{
    double rho = provider.GetPressure("TopNorth"); 
    double corr = (1./4.) * (comp[E]-comp[W]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[S ] = comp[N ];
    comp[B ] = comp[T ];
    comp[BS] = comp[TN];

    comp[SW] = comp[NE] + corr;
    comp[SE] = comp[NW] - corr;
    comp[BW] = comp[TE] + corr;
    comp[BE] = comp[TW] - corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[NE] + comp[SW] + comp[SE] + comp[TN] + comp[TW] 
                     + comp[TE] + comp[BS] + comp[BW] + comp[BE] );

    erg = (rho - 22.*erg)/14.; 

    comp[TS] = erg;
    comp[BN] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeTopEastPressure( const COORD_MAP& neighborhood )
{
    double rho = provider.GetPressure("TopEast"); 
    double corr = (1./4.) * (comp[N]-comp[S]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[W ] = comp[E ];
    comp[B ] = comp[T ];
    comp[BW] = comp[TE];

    comp[NW] = comp[SE] - corr;
    comp[SW] = comp[NE] + corr;
    comp[BN] = comp[TS] - corr;
    comp[BS] = comp[TN] + corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[NE] + comp[SW] + comp[SE] + comp[TN] + comp[TS] 
                     + comp[TE] + comp[BN] + comp[BS] + comp[BW] );

    erg = (rho - 22.*erg)/14.; 

    comp[BE] = erg;
    comp[TW] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeTopWestPressure( const COORD_MAP& neighborhood )
{
    double rho = provider.GetPressure("TopWest"); 
    double corr = (1./4.) * (comp[N]-comp[S]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[E ] = comp[W ];
    comp[B ] = comp[T ];
    comp[BE] = comp[TW];

    comp[SE] = comp[NW] + corr;
    comp[NE] = comp[SW] - corr;
    comp[BN] = comp[TS] - corr;
    comp[BS] = comp[TN] + corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ E] + comp[ W] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[NE] + comp[SW] + comp[SE] + comp[TN] + comp[TS] 
                     + comp[TW] + comp[BN] + comp[BS] + comp[BE] );

    erg = (rho - 22.*erg)/14.; 

    comp[TE] = erg;
    comp[BW] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeSouthWestPressure( const COORD_MAP& neighborhood )
{
    double rho = provider.GetPressure("SouthWest"); 
    double corr = (1./4.) * (comp[T]-comp[B]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[E ] = comp[W ];
    comp[N ] = comp[S ];
    comp[NE] = comp[SW];

    comp[TE] = comp[BW] - corr;
    comp[BE] = comp[TW] + corr;
    comp[TN] = comp[BS] - corr;
    comp[BN] = comp[TS] + corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NE] + comp[SW] + comp[TN] + comp[TS] + comp[TW] + comp[TE] 
                     + comp[BN] + comp[BS] + comp[BW] + comp[BE] );

    erg = (rho - 22.*erg)/14.; 

    comp[SE] = erg;
    comp[NW] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeSouthEastPressure( const COORD_MAP& neighborhood )
{
    double rho = provider.GetPressure("SouthEast"); 
    double corr = (1./4.) * (comp[T]-comp[B]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[W ] = comp[E ];
    comp[N ] = comp[S ];
    comp[NW] = comp[SE];

    comp[BW] = comp[TE] + corr;
    comp[TW] = comp[BE] - corr;
    comp[TN] = comp[BS] - corr;
    comp[BN] = comp[TS] + corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NW] + comp[SE] + comp[TN] + comp[TS] + comp[TW] + comp[TE] 
                     + comp[BN] + comp[BS] + comp[BW] + comp[BE] );

    erg = (rho - 22.*erg)/14.; 

    comp[NE] = erg;
    comp[SW] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeNorthWestPressure( const COORD_MAP& neighborhood )
{
    double rho = provider.GetPressure("NorthWest"); 
    double corr = (1./4.) * (comp[T]-comp[B]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[E ] = comp[W ];
    comp[S ] = comp[N ];
    comp[SE] = comp[NW];

    comp[TE] = comp[BW] - corr;
    comp[BE] = comp[TW] + corr;
    comp[BS] = comp[TN] + corr;
    comp[TS] = comp[BN] - corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                   + comp[NW] + comp[SE] + comp[TN] + comp[TS] + comp[TW] + comp[TE] 
                   + comp[BN] + comp[BS] + comp[BW] + comp[BE] );

    erg = (rho - 22.*erg)/14.; 

    comp[NE] = erg;
    comp[SW] = erg;
    comp[C] = f3*erg;
}

template<typename COORD_MAP>
void Cell::EdgeNorthEastPressure( const COORD_MAP& neighborhood )
{
    double rho = provider.GetPressure("NorthEast"); 
    double corr = (1./4.) * (comp[T]-comp[B]);
    double f2 = 1./22.;
    double f3 = 12.;

    comp[W ] = comp[E ];
    comp[S ] = comp[N ];
    comp[SW] = comp[NE];

    comp[BW] = comp[TE] + corr;
    comp[TW] = comp[BE] - corr;
    comp[BS] = comp[TN] + corr;
    comp[TS] = comp[BN] - corr;

    double erg =  f2 * ( comp[ N] + comp[ S] + comp[ W] + comp[ E] + comp[ T] + comp[ B] 
                     + comp[NE] + comp[SW] + comp[TN] + comp[TS] + comp[TW] + comp[TE] 
                     + comp[BN] + comp[BS] + comp[BW] + comp[BE] );

    erg = (rho - 22.*erg)/14.; 

    comp[SE] = erg;
    comp[NW] = erg;
    comp[C] = f3*erg;
}
//================================================================================================


template<typename COORD_MAP>
void Cell::CornerBottomSouthWestVelocity( const COORD_MAP& neighborhood )
{
    double f3 = 1./18.;

    comp[E ] = comp[W];
    comp[N ] = comp[S];
    comp[T ] = comp[B];
    comp[NE] = comp[SW];
    comp[TE] = comp[BW];
    comp[TN] = comp[BS];

    double erg = f3 * ( comp[N] + comp[S] + comp[E] +comp[W] + comp[T] +comp[B] + comp[NE] +comp[SW] + comp[BW] +comp[TE] + comp[TN] + comp[BS] );
  
    comp[SE] = erg;
    comp[NW] = erg;
    comp[BE] = erg;
    comp[TW] = erg;
    comp[BN] = erg;
    comp[TS] = erg;

    comp[ C] = 12. * erg;

}

template<typename COORD_MAP>
void Cell::CornerBottomSouthEastVelocity( const COORD_MAP& neighborhood )
{
   double f3 = 1./18.;

    comp[ W] = comp[ E];
    comp[ N] = comp[ S];
    comp[ T] = comp[ B];
    comp[NW] = comp[SE];
    comp[TW] = comp[BE];
    comp[TN] = comp[BS];

    double erg = f3 * ( comp[N] + comp[S] + comp[W] +comp[E] + comp[T] +comp[B] + comp[NW] +comp[SE] + comp[TW] +comp[TN] + comp[BE] + comp[BS] );

    comp[NE] = erg;
    comp[SW] = erg;
    comp[TE] = erg;
    comp[BW] = erg;
    comp[BN] = erg;
    comp[TS] = erg;

    comp[C] = 12. * erg;

}

template<typename COORD_MAP>
void Cell::CornerBottomNorthEastVelocity( const COORD_MAP& neighborhood )
{
    double f3 = 1./18.;

    comp[ W] = comp[ E];
    comp[ S] = comp[ N];
    comp[ T] = comp[ B];
    comp[SW] = comp[NE];
    comp[TW] = comp[BE];
    comp[TS] = comp[BN];

    double erg = f3 * ( comp[N] + comp[S] + comp[W] +comp[E] + comp[T] +comp[B] + comp[NE] +comp[SW] + comp[TW] +comp[TS] + comp[BE] + comp[BN] );

    comp[SE] = erg;
    comp[NW] = erg;
    comp[TE] = erg;
    comp[BW] = erg;
    comp[TN] = erg;
    comp[BS] = erg;

    comp[C] = 12. * erg;

}

template<typename COORD_MAP>
void Cell::CornerBottomNorthWestVelocity( const COORD_MAP& neighborhood )
{
    double f3 = 1./18.;

    comp[E ] = comp[W ];
    comp[S ] = comp[N ];
    comp[T ] = comp[B ];
    comp[SE] = comp[NW];
    comp[TE] = comp[BW];
    comp[TS] = comp[BN];

    double erg = f3 * ( comp[N] + comp[S] + comp[W] +comp[E] + comp[T] +comp[B] + comp[NW] +comp[SE] + comp[TE] +comp[TS] + comp[BW] + comp[BN] );

    comp[NE] = erg;
    comp[SW] = erg;
    comp[BE] = erg;
    comp[TW] = erg;
    comp[TN] = erg;
    comp[BS] = erg;

    comp[C] = 12. * erg;

}

template<typename COORD_MAP>
void Cell::CornerTopSouthWestVelocity( const COORD_MAP& neighborhood )
{
    double f3 = 1./18.;

    comp[E ] = comp[W ];
    comp[N ] = comp[S];
    comp[B ] = comp[T ];
    comp[NE] = comp[SW];
    comp[BE] = comp[TW];
    comp[BN] = comp[TS];

    double erg = f3 * ( comp[N] + comp[S] + comp[W] +comp[E] + comp[T] +comp[B] + comp[NE] +comp[SW] + comp[TW] +comp[TS] + comp[BE] + comp[BN] );

    comp[SE] = erg;
    comp[NW] = erg;
    comp[TE] = erg;
    comp[BW] = erg;
    comp[TN] = erg;
    comp[BS] = erg;

    comp[C] = 12. * erg;

}

template<typename COORD_MAP>
void Cell::CornerTopSouthEastVelocity( const COORD_MAP& neighborhood )
{
    double f3 = 1./18.;

    comp[W ] = comp[E ];
    comp[N ] = comp[S ];
    comp[B ] = comp[T ];
    comp[NW] = comp[SE];
    comp[BW] = comp[TE];
    comp[BN] = comp[TS];

    double erg = f3 * ( comp[N] + comp[S] + comp[W] +comp[E] + comp[T] +comp[B] + comp[NW] +comp[SE] + comp[TE] +comp[TS] + comp[BW] + comp[BN] );

    comp[NE] = erg;
    comp[SW] = erg;
    comp[BE] = erg;
    comp[TW] = erg;
    comp[TN] = erg;
    comp[BS] = erg;

    comp[C] = 12. * erg;

}

template<typename COORD_MAP>
void Cell::CornerTopNorthEastVelocity( const COORD_MAP& neighborhood )
{
    double f3 = 1./18.;

    comp[W ] = comp[E ];
    comp[S ] = comp[N ];
    comp[B ] = comp[T ];
    comp[SW] = comp[NE];
    comp[BW] = comp[TE];
    comp[BS] = comp[TN];

    double  erg = f3 * ( comp[N] + comp[S] + comp[W] +comp[E] + comp[T] + comp[B] + comp[NE] + comp[SW] + comp[TE] + comp[TN] + comp[BW] + comp[BS] );

    comp[SE] = erg;
    comp[NW] = erg;
    comp[BE] = erg;
    comp[TW] = erg;
    comp[BN] = erg;
    comp[TS] = erg;

    comp[C] = 12. * erg;

}

template<typename COORD_MAP>
void Cell::CornerTopNorthWestVelocity( const COORD_MAP& neighborhood )
{
    double f3 = 1./18.;

    comp[E ] = comp[W ];
    comp[S ] = comp[N ];
    comp[B ] = comp[T ];
    comp[SE] = comp[NW];
    comp[BE] = comp[TW];
    comp[BS] = comp[TN];

    double erg = f3 * ( comp[N] + comp[S] + comp[W] +comp[E] + comp[T] +comp[B] + comp[NW] +comp[SE] + comp[TW] +comp[TN] + comp[BE] + comp[BS] );

    comp[NE] = erg;
    comp[SW] = erg;
    comp[TE] = erg;
    comp[BW] = erg;
    comp[BN] = erg;
    comp[TS] = erg;

    comp[C] = 12. * erg;
}
//==================================================================================

template<typename COORD_MAP>
void Cell::CornerBottomSouthWestPressure( const COORD_MAP& neighborhood )
{
    double f3 = 1./18.;
    double rho = provider.GetPressure("BottomSouthWest");

    comp[E ] = comp[W];
    comp[N ] = comp[S];
    comp[T ] = comp[B];
    comp[NE] = comp[SW];
    comp[TE] = comp[BW];
    comp[TN] = comp[BS];

    double erg = f3 * ( comp[N] + comp[S] + comp[E] +comp[W] + comp[T] +comp[B] + comp[NE] +comp[SW] + comp[BW] +comp[TE] + comp[TN] + comp[BS] );
    erg = rho/18. - erg;
  
    comp[SE] = erg;
    comp[NW] = erg;
    comp[BE] = erg;
    comp[TW] = erg;
    comp[BN] = erg;
    comp[TS] = erg;

    comp[ C] = 12. * erg;

}

template<typename COORD_MAP>
void Cell::CornerBottomSouthEastPressure( const COORD_MAP& neighborhood )
{
 
   double f3 = 1./18.;
   double rho = provider.GetPressure("BottomSouthEast");

    comp[ W] = comp[ E];
    comp[ N] = comp[ S];
    comp[ T] = comp[ B];
    comp[NW] = comp[SE];
    comp[TW] = comp[BE];
    comp[TN] = comp[BS];

    double erg = f3 * ( comp[N] + comp[S] + comp[W] +comp[E] + comp[T] +comp[B] + comp[NW] +comp[SE] + comp[TW] +comp[TN] + comp[BE] + comp[BS] );
    erg = rho/18. - erg;

    comp[NE] = erg;
    comp[SW] = erg;
    comp[TE] = erg;
    comp[BW] = erg;
    comp[BN] = erg;
    comp[TS] = erg;

    comp[C] = 12. * erg;

}

template<typename COORD_MAP>
void Cell::CornerBottomNorthEastPressure( const COORD_MAP& neighborhood )
{

    double f3 = 1./18.;
    double rho = provider.GetPressure("BottomNorthEast");

    comp[ W] = comp[ E];
    comp[ S] = comp[ N];
    comp[ T] = comp[ B];
    comp[SW] = comp[NE];
    comp[TW] = comp[BE];
    comp[TS] = comp[BN];

    double erg = f3 * ( comp[N] + comp[S] + comp[W] +comp[E] + comp[T] +comp[B] + comp[NE] +comp[SW] + comp[TW] +comp[TS] + comp[BE] + comp[BN] );
    erg = rho/18. - erg;

    comp[SE] = erg;
    comp[NW] = erg;
    comp[TE] = erg;
    comp[BW] = erg;
    comp[TN] = erg;
    comp[BS] = erg;

    comp[C] = 12. * erg;

}

template<typename COORD_MAP>
void Cell::CornerBottomNorthWestPressure( const COORD_MAP& neighborhood )
{

    double f3 = 1./18.;
    double rho = provider.GetPressure("BottomNorthWest");

    comp[E ] = comp[W ];
    comp[S ] = comp[N ];
    comp[T ] = comp[B ];
    comp[SE] = comp[NW];
    comp[TE] = comp[BW];
    comp[TS] = comp[BN];

    double erg = f3 * ( comp[N] + comp[S] + comp[W] +comp[E] + comp[T] +comp[B] + comp[NW] +comp[SE] + comp[TE] +comp[TS] + comp[BW] + comp[BN] );
    erg = rho/18. - erg;

    comp[NE] = erg;
    comp[SW] = erg;
    comp[BE] = erg;
    comp[TW] = erg;
    comp[TN] = erg;
    comp[BS] = erg;

    comp[C] = 12. * erg;

}

template<typename COORD_MAP>
void Cell::CornerTopSouthWestPressure( const COORD_MAP& neighborhood )
{

    double f3 = 1./18.;
    double rho = provider.GetPressure("TopSouthWest");

    comp[E ] = comp[W ];
    comp[N ] = comp[S];
    comp[B ] = comp[T ];
    comp[NE] = comp[SW];
    comp[BE] = comp[TW];
    comp[BN] = comp[TS];

    double erg = f3 * ( comp[N] + comp[S] + comp[W] +comp[E] + comp[T] +comp[B] + comp[NE] +comp[SW] + comp[TW] +comp[TS] + comp[BE] + comp[BN] );
    erg = rho/18. - erg;

    comp[SE] = erg;
    comp[NW] = erg;
    comp[TE] = erg;
    comp[BW] = erg;
    comp[TN] = erg;
    comp[BS] = erg;

    comp[C] = 12. * erg;

}

template<typename COORD_MAP>
void Cell::CornerTopSouthEastPressure( const COORD_MAP& neighborhood )
{

    double f3 = 1./18.;
    double rho = provider.GetPressure("TopSouthEast");

    comp[W ] = comp[E ];
    comp[N ] = comp[S ];
    comp[B ] = comp[T ];
    comp[NW] = comp[SE];
    comp[BW] = comp[TE];
    comp[BN] = comp[TS];

    double erg = f3 * ( comp[N] + comp[S] + comp[W] +comp[E] + comp[T] +comp[B] + comp[NW] +comp[SE] + comp[TE] +comp[TS] + comp[BW] + comp[BN] );
    erg = rho/18. - erg;

    comp[NE] = erg;
    comp[SW] = erg;
    comp[BE] = erg;
    comp[TW] = erg;
    comp[TN] = erg;
    comp[BS] = erg;

    comp[C] = 12. * erg;

}

template<typename COORD_MAP>
void Cell::CornerTopNorthEastPressure( const COORD_MAP& neighborhood )
{

    double f3 = 1./18.;
    double rho = provider.GetPressure("TopNorthEast");

    comp[W ] = comp[E ];
    comp[S ] = comp[N ];
    comp[B ] = comp[T ];
    comp[SW] = comp[NE];
    comp[BW] = comp[TE];
    comp[BS] = comp[TN];

    double  erg = f3 * ( comp[N] + comp[S] + comp[W] +comp[E] + comp[T] + comp[B] + comp[NE] + comp[SW] + comp[TE] + comp[TN] + comp[BW] + comp[BS] );
    erg = rho/18. - erg;

    comp[SE] = erg;
    comp[NW] = erg;
    comp[BE] = erg;
    comp[TW] = erg;
    comp[BN] = erg;
    comp[TS] = erg;

    comp[C] = 12. * erg;

}


template<typename COORD_MAP>
void Cell::CornerTopNorthWestPressure( const COORD_MAP& neighborhood )
{
    double f3 = 1./18.;
    double erg = 0.0;
    double rho = provider.GetPressure("TopNorthWest");

    comp[E ] = comp[W ];
    comp[S ] = comp[N ];
    comp[B ] = comp[T ];
    comp[SE] = comp[NW];
    comp[BE] = comp[TW];
    comp[BS] = comp[TN];

    erg = f3 * ( comp[N] + comp[S] + comp[W] +comp[E] + comp[T] +comp[B] + comp[NW] +comp[SE] + comp[TW] +comp[TN] + comp[BE] + comp[BS] );
    erg = rho/18. - erg;

    comp[NE] = erg;
    comp[SW] = erg;
    comp[TE] = erg;
    comp[BW] = erg;
    comp[BN] = erg;
    comp[TS] = erg;

    comp[C] = 12. * erg;
}

#endif
