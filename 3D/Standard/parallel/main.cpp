#include <libgeodecomp/io/bovwriter.h>
#include <libgeodecomp/io/simpleinitializer.h>
#include <libgeodecomp/io/tracingwriter.h>
#include <libgeodecomp/loadbalancer/tracingbalancer.h>
#include <libgeodecomp/loadbalancer/noopbalancer.h>
#include <libgeodecomp/parallelization/stripingsimulator.h>


#include "../src/Cell.h"
#include "../src/Provider.h"
#include "../src/CellInitializer.h"
#include "../src/SelectorBOV.h"
#include "../src/SelectorDrag.h"
#include "../src/FileReader.h"
#include "../src/Constants.h"
#include "../src/DragForceWriter.h"
#include "../src/Geometry.h"
#include "../src/Box.h"
#include "../src/Sphere.h"
#include "../src/Cylinder.h"


using namespace LibGeoDecomp;


void SetBoundaries(FileReader* Reader, CellInitializer* cellinit){
    std::map<string,string> boundary;

    boundary["Top"]    = Reader->GetType("Top") ;
    boundary["Bottom"] = Reader->GetType("Bottom") ;
    boundary["North"]  = Reader->GetType("North") ;
    boundary["South"]  = Reader->GetType("South") ;
    boundary["East"]   = Reader->GetType("East") ;
    boundary["West"]   = Reader->GetType("West") ;

    boundary["TopNorth"]    = Reader->GetType("TopNorth") ;
    boundary["TopSouth"]    = Reader->GetType("TopSouth") ;
    boundary["TopEast"]     = Reader->GetType("TopEast") ;
    boundary["TopWest"]     = Reader->GetType("TopWest") ;
    boundary["BottomNorth"] = Reader->GetType("BottomNorth") ;
    boundary["BottomSouth"] = Reader->GetType("BottomSouth") ;
    boundary["BottomEast"]  = Reader->GetType("BottomEast") ;
    boundary["BottomWest"]  = Reader->GetType("BottomWest") ;
    boundary["NorthEast"]   = Reader->GetType("NorthEast") ;
    boundary["NorthWest"]   = Reader->GetType("NorthWest") ;
    boundary["SouthEast"]   = Reader->GetType("SouthEast") ;
    boundary["SouthWest"]   = Reader->GetType("SouthWest"); 

    boundary["TopNorthEast"]    = Reader->GetType("TopNorthEast") ;
    boundary["TopNorthWest"]    = Reader->GetType("TopNorthWest") ;
    boundary["TopSouthEast"]    = Reader->GetType("TopSouthEast") ;
    boundary["TopSouthWest"]    = Reader->GetType("TopSouthWest") ;
    boundary["BottomNorthEast"] = Reader->GetType("BottomNorthEast") ;
    boundary["BottomNorthWest"] = Reader->GetType("BottomNorthWest") ;
    boundary["BottomSouthEast"] = Reader->GetType("BottomSouthEast") ;
    boundary["BottomSouthWest"] = Reader->GetType("BottomSouthWest") ;

    cellinit->SetBoundary(boundary);

}

void SetObstacles( CellInitializer* cellinit, FileReader* Reader ){

    vector<BoxStorage> obstacleBox = Reader->GetBoxes();
    vector<SphereStorage> obstacleSphere = Reader->GetSpheres();
    vector<CylinderStorage> obstacleCylinder = Reader->GetCylinders();

    std::vector<BoxStorage>::iterator itBox;
    for( itBox=obstacleBox.begin(); itBox != obstacleBox.end(); ++itBox){
        cellinit->SetBox(itBox->M_X, itBox->M_Y, itBox->M_Z, itBox->l_X, itBox->l_Y, itBox->l_Z);
    }
  
    std::vector<CylinderStorage>::iterator itCyl;
    for( itCyl=obstacleCylinder.begin(); itCyl != obstacleCylinder.end(); ++itCyl ){
        cellinit->SetCylinder(itCyl->P1_X, itCyl->P1_Y, itCyl->P1_Z, itCyl->P2_X, itCyl->P2_Y, itCyl->P2_Z, itCyl->h, itCyl->r );
    }

    std::vector<SphereStorage>::iterator itSph;
    for( itSph=obstacleSphere.begin(); itSph != obstacleSphere.end(); ++itSph ){
        cellinit->SetSphere(itSph->M_X, itSph->M_Y, itSph->M_Z, itSph->r  );
    }




}

void SetWriter(StripingSimulator<Cell>* sim, FileReader* Reader)
{

    const string outputvelocity = Reader->GetValue<string>("OutputVelocity");
    const string outputdensity  = Reader->GetValue<string>("OutputDensity");
    const string outputflag = Reader->GetValue<string>("OutputFlag");    
    const string outputdrag = Reader->GetValue<string>("OutputDrag");

    const int printstep = Reader->GetValue<int>("printstep");
    const int tracestep = Reader->GetValue<int>("tracestep");
    const int timesteps = Reader->GetValue<int>("timesteps"); 

    if(outputvelocity != "NULL" ){
        sim->addWriter( new BOVWriter<Cell, VelocitySelector>(outputvelocity+".velocity",printstep) );
    }
    if(outputdensity != "NULL" ){
        sim->addWriter( new BOVWriter<Cell, DensitySelector>(outputdensity+".density",printstep) );
    }
    if(outputflag != "NULL" ){
        sim->addWriter( new BOVWriter<Cell, FlagSelector>(outputflag+".flag",printstep) );
    }
    if(outputdrag != "NULL" ){
        sim->addWriter( new DragForceWriter<Cell, DragForceSelector>(outputdrag+".drag",printstep) );
    }

    if (MPILayer().rank() == 0){
        sim->addWriter( new TracingWriter<Cell>(tracestep,timesteps) );
    }

}


void runSimulation(CellInitializer* cellinit, FileReader* Reader)
{
    MPI::Aint displacements[] = {0};
    MPI::Datatype memberTypes[] = {MPI::CHAR};
    int lengths[] = {sizeof(Cell)};
    MPI::Datatype objType;
    objType = MPI::Datatype::Create_struct(1, lengths, displacements, memberTypes);
    objType.Commit();

    StripingSimulator<Cell> sim( cellinit, MPILayer().rank() ? 0 : new TracingBalancer(new NoOpBalancer()), 1000000, objType);
    SetWriter(&sim, Reader );

    sim .run();

} 

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);
    Typemaps::initializeMaps();

    string inputfile;
  
    if(argc != 2){
        cerr << "Missing Parameter File!" << endl;
        exit(1);
    }
    inputfile = argv[1];
    FileReader Reader;
    Reader.read( inputfile );
    const int sizeX     = Reader.GetValue<int>("sizeX");
    const int sizeY     = Reader.GetValue<int>("sizeY");
    const int sizeZ     = Reader.GetValue<int>("sizeZ");
    const int timesteps = Reader.GetValue<int>("timesteps");

    CellInitializer *cellinit = new CellInitializer( Coord<3>(sizeX,sizeY,sizeZ), timesteps );
    SetBoundaries(&Reader,cellinit);
    SetObstacles(cellinit, &Reader);
    runSimulation(cellinit, &Reader);

    MPI_Finalize();
    return 0;
}
