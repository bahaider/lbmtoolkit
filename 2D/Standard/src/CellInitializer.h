#ifndef _CELLINITIALIZER_H_
#define _CELLINITIALIZER_H_

#include <libgeodecomp/io/simpleinitializer.h>

#include "Cell.h"
#include "../../util/ImageReader.h"
#include "../../util/Circle.h"
#include "../../util/Rectangle.h"
#include "../../util/Triangle.h"
#include "../../util/EnumParser.h"
#include "../../util/StorageStructs.h"

class CellInitializer : public SimpleInitializer<Cell>
{
private:
    map<string,string> m_boundary;
    vector<Geometry*> m_obstacle;
    vector<int> m_imagevalues;
    string m_imagepath; 

    int CheckBounceBack( string input, int bounceback ){
        map<string,string>::iterator iter;
        iter = m_boundary.find(input);
        if(bounceback == 1){ return 1;}
        else if( iter->second == "NO_SLIP" ){ return 1;}
        else if( iter->second == "FREE_SLIP_N_S" ){ return 1;}
        else if( iter->second == "FREE_SLIP_E_W" ){ return 1;}
        else if( iter->second == "ACCELERATION"  ){ return 1;}
        else{ return 0;}  
    }

    int CheckObstacleBound(int x, int y){
        
        for(std::vector<Geometry*>::iterator it=m_obstacle.begin(); it != m_obstacle.end();++it){
            if((*it)->IsBoundingBox(x,y) == 1) return 1;
        }
        return 0;
    }

    bool CheckObstaclePoint(int x, int y){

        for(std::vector<Geometry*>::iterator it=m_obstacle.begin(); it != m_obstacle.end();++it){
            if((*it)->IsInside(x,y) == 1) return 1;
        }
        return 0;
    }


public:
    CellInitializer(Coord<2> dim, int maxSteps) : SimpleInitializer<Cell>(dim, maxSteps)
    {
        m_obstacle.clear();
        m_boundary.clear();
        m_imagevalues.clear();
        m_imagepath = "dummy";
    }

    void SetBoundary( map<string,string> boundary )
    {
        m_boundary = boundary;
    }

    void SetImage(string image)
    {
       m_imagepath = image;
    }

    void SetCircle(int mx, int my, int r)
    {
         m_obstacle.push_back( new Circle(mx,my,r) );
    }
    void SetRectangle(int mx, int my, int lx, int ly)
    {
         m_obstacle.push_back( new Rectangle(mx, my, lx,ly) );
    }

    void SetTriangle(int p1x, int p1y, int p2x, int p2y, int r, int h)
    {
         m_obstacle.push_back( new Triangle(p1x,p1y,p2x,p2y,r,h) );
    }

    virtual void grid(GridBase<Cell, 2> *ret)
    {
        CoordBox<2> box = ret->boundingBox();
        Coord<2> size = this->gridDimensions();
        EnumParser<State> parser;
        map<string,string>::iterator it;

        int xE = size.x()-1;
        int yE = size.y()-1;

        if( m_imagepath != "NULL" ){     
           ImageReader image(m_imagepath,size.x(),size.y());
           m_imagevalues = image.GetAllValues();
        }

	for (int y = 0; y < size.y(); ++y) {
            for (int x = 0; x < size.x(); ++x) {
                Coord<2> c(x, y);
                State s = FLUID;
                int bb = 0;

                    if (c.y() == 0 ){
                        it = m_boundary.find("South");
                        s  = parser.ParseEnum( it->second );
                    } 
                    if (c.y() == yE){
                        it = m_boundary.find("North");
                        s  = parser.ParseEnum( it->second );
                    }
                    if (c.x() == 0 ){
                        it = m_boundary.find("West");
                        s  = parser.ParseEnum( it->second );
                    }
                    if (c.x() == xE){
                        it = m_boundary.find("East");
                        s  = parser.ParseEnum( it->second );
                    }

                    
                    // ===== Corner =====
                    if( c.y() == yE && c.x() == xE ){
                        it = m_boundary.find("NorthEast");
                        s = parser.ParseEnum( it->second );
                    }
                    if( c.y() == yE && c.x() == 0  ){
                        it = m_boundary.find("NorthWest");
                        s = parser.ParseEnum( it->second );
                    } 
                    if( c.y() == 0  && c.x() == xE ){
                        it = m_boundary.find("SouthEast");
                        s = parser.ParseEnum( it->second );
                    }
                    if( c.y() == 0  && c.x() == 0  ){
                        it = m_boundary.find("SouthWest");
                        s = parser.ParseEnum( it->second );
                    }

                    //==== Bounceback ====
                    if(c.y() == yE-1){
                        bb = CheckBounceBack("North", bb);
                    } 
                    if(c.y() == 1   ){
                        bb = CheckBounceBack("South", bb);
                    } 
                    if(c.x() == xE-1){
                        bb = CheckBounceBack("East", bb);
                    } 
                    if(c.x() == 1   ){
                        bb = CheckBounceBack("West", bb);
                    } 
                    


                    if (CheckObstacleBound( c.x(),c.y() ) == 1){
                        bb = 1;
                    } 

                    if( CheckObstaclePoint( c.x(),c.y() ) == 1){
                        bb = 0;
                        s = OBSTACLE;
                    }

                    if (m_imagepath != "NULL"){
                        int greyVal = m_imagevalues[c.y()*size.x()+c.x()];
                        if(greyVal == 0){ s = OBSTACLE; }
                        else if(greyVal != 255){bb = 1;}
                        else{ }
                    }

                if (box.inBounds(c))
                    ret->at(c) = Cell(s,x,y,bb);

            }
        }
    }
};

#endif
