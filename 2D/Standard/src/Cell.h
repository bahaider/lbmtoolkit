#ifndef _CELL_H_
#define _CELL_H_

#include "../../util/Constants.h"
#include "../../util/Provider.h"
#include "math.h"

#define GET_STATE(X, Y) neighborhood[Coord<2>(X, Y)].state
#define GET_COMP(X, Y, COMP) neighborhood[Coord<2>(X, Y)].comp[COMP]
#define neigh(X,Y) neighborhood[Coord<2>(X,Y)] 
#define SQR(X) ((X)*(X))

using namespace LibGeoDecomp;

class Cell
{
public:
    typedef Stencils::Moore<2, 1> Stencil;
    typedef Topologies::Torus<2>::Topology Topology;
    class API : public CellAPITraits::Base
    {};


    static inline unsigned nanoSteps()
    {
        return 1;
    }

    inline explicit Cell( const State& s = DUMMY, const int& px = 9999, const int& py = 99999, const int& bb = 9999) :
        state(s)
    {
	bounceback = bb;
        posx = px;
        posy = py;    
        iter = 1;
	density   = provider.GetPressure();
	velocityX = provider.GetInitVelocity_L( 'x' );
	velocityY = provider.GetInitVelocity_L( 'y' );
	
	for(int i = 0; i < CELLSIZE; ++i){
		double cu = 3.0*(g_cx[i]*velocityX + g_cy[i]*velocityY);
		comp[i] = g_weight[i]*density*(1.0 + cu + (1./2.) * cu*cu - (3./2.)*(velocityX*velocityX + velocityY*velocityY));
	}
   }
        
    template<typename COORD_MAP>
    void update(const COORD_MAP& neighborhood, const unsigned& nanoStep)
    {

        GridExchange( neighborhood );
		
        if( nanoStep == 0 ){

            StreamCollide(neighborhood);

            if(bounceback == 1){
                BounceBackBoundaries( neighborhood );
            }

            switch (state){            
                case ZOU_HE_NORTH_VELOCITY:
                    ZouHeVelocityNorth( neighborhood );
                    break;
                case ZOU_HE_SOUTH_VELOCITY:
                    ZouHeVelocitySouth( neighborhood );
                    break;
                case ZOU_HE_WEST_VELOCITY:
                    ZouHeVelocityWest( neighborhood );
                    break;
                case ZOU_HE_EAST_VELOCITY:
                    ZouHeVelocityEast( neighborhood );               
                    break;
                case ZOU_HE_EAST_PRESSURE:
                    ZouHePressureEast( neighborhood );               
                    break;
                case ZOU_HE_WEST_PRESSURE:
                    ZouHePressureWest( neighborhood );               
                    break;
                case ZOU_HE_NORTH_PRESSURE:
                    ZouHePressureNorth( neighborhood );               
                    break;
                case ZOU_HE_SOUTH_PRESSURE:
                    ZouHePressureSouth( neighborhood );               
                    break;
                case CORNER_NORTH_EAST_VELOCITY:
                    ZouHeVelocityCornerNorthEast( neighborhood );               
                    break;
                case CORNER_NORTH_WEST_VELOCITY:
                    ZouHeVelocityCornerNorthWest( neighborhood );               
                    break;
                case CORNER_SOUTH_EAST_VELOCITY:
                    ZouHeVelocityCornerSouthEast( neighborhood );               
                    break;
                case CORNER_SOUTH_WEST_VELOCITY:
                    ZouHeVelocityCornerSouthWest( neighborhood );               
                    break;
                case CORNER_NORTH_EAST_PRESSURE:
                    ZouHePressureCornerNorthEast( neighborhood );               
                    break;
                case CORNER_NORTH_WEST_PRESSURE:
                    ZouHePressureCornerNorthWest( neighborhood );               
                    break;
                case CORNER_SOUTH_EAST_PRESSURE:
                    ZouHePressureCornerSouthEast( neighborhood );               
                    break;
                case CORNER_SOUTH_WEST_PRESSURE:
                    ZouHePressureCornerSouthWest( neighborhood );               
                    break;
                default:
                    break;
            }

            iter = iter+1;
            calcMacroscopicValues( neighborhood );
	}
    }
	
    template <typename COORD_MAP>
    void StreamCollide( const COORD_MAP& neighborhood )
    {

        //================================================================
        double tau = provider.GetTau();
        double gx  = provider.GetGravityX()*tau; 
        double gy  = provider.GetGravityY()*tau;
        //================================================================
 
        //===================================================
        double forceX = provider.GetForceX();
        double forceY = provider.GetForceY();
        //====================================================
		
        double usq   = 0.0;
        double uXsq  = 0.0;
        double uYsq  = 0.0;
        double rt1   = 0.0;
        double rt2   = 0.0;
        double shift = 0.0;

        const double f1 =  3.0;
        const double f2 = (9.0/2.0);
        const double f3 = (3.0/2.0);
        const double omega = provider.CalcOmega();
        const double omega_trm = 1.0-omega;

        //===== CENTER =====
        double rho   = neigh(0,0).density; 
	double velX  = neigh(0,0).velocityX + gx;
	double velY  = neigh(0,0).velocityY + gy;
	
        usq = (velX*velX + velY*velY);

        comp[C ] = omega_trm * GET_COMP(0,0,C) + omega * ( (4./9.)*rho * (1. - f3*usq) ) + 3.*g_weight[C]*rho*(g_cx[C]*forceX + g_cy[C]*forceY);
		
        //===== SOUTH =====
        rho   = neigh(0,1).density;
        velX  = neigh(0,1).velocityX + gx;
        velY  = neigh(0,1).velocityY + gy;
		
        usq = (velX*velX + velY*velY);
        uYsq = SQR(velY);
        rt1   = (1./9. )*rho;
		
        shift = omega_trm * GET_COMP( 0,1, S) + omega * ( rt1 * (1. - f1*velY + f2*uYsq - f3*usq) ) + 3.*g_weight[S]*rho*(g_cx[S]*forceX + g_cy[S]*forceY);
        comp[S ] = shift;

        //===== NORTH =====
        rho   = neigh(0,-1).density;
        velX  = neigh(0,-1).velocityX + gx;
        velY  = neigh(0,-1).velocityY + gy; 
		
        usq = (velX*velX + velY*velY);
        uYsq = SQR(velY);
        rt1   = (1./9. )*rho;
        shift =  omega_trm * GET_COMP( 0,-1, N) + omega * ( rt1 * (1. + f1*velY + f2*uYsq - f3*usq) ) + 3.*g_weight[N]*rho*(g_cx[N]*forceX + g_cy[N]*forceY);
        comp[N ] = shift;

        //===== WEST =====
        rho   = neigh(1,0).density; 
        velX  = neigh(1,0).velocityX + gx;
        velY  = neigh(1,0).velocityY + gy;
		
        usq = (velX*velX + velY*velY);
        uXsq = SQR(velX); 
        rt1 = (1./9.)*rho;

        shift = omega_trm * GET_COMP(1, 0, W) + omega * ( rt1 * (1. - f1*velX + f2*uXsq - f3*usq) ) + 3.*g_weight[W]*rho*(g_cx[W]*forceX + g_cy[W]*forceY);
        comp[W ] = shift;

        //===== EAST =====
        rho   = neigh(-1,0).density; 
        velX  = neigh(-1,0).velocityX + gx;
        velY  = neigh(-1,0).velocityY + gy;
		
        usq = (velX*velX + velY*velY);
        uXsq = SQR(velX); 
        rt1 = (1./9.)*rho;

        shift = omega_trm * GET_COMP(-1,0,E) + omega * ( rt1 * (1. + f1*velX + f2*uXsq - f3*usq) ) + 3.*g_weight[E]*rho*(g_cx[E]*forceX + g_cy[E]*forceY);
        comp[E ] = shift;

        //===== NORTH EAST =====
        rho   = neigh(-1,-1).density; 
        velX  = neigh(-1,-1).velocityX + gx;
        velY  = neigh(-1,-1).velocityY + gy;
		
        usq = (velX*velX + velY*velY); 
        double uxuyNE =  velX + velY;
        rt2 = (1./36.)*rho;

        shift = omega_trm * GET_COMP(-1,-1,NE) + omega * (rt2 * (1. + f1*uxuyNE + f2*uxuyNE*uxuyNE - f3*usq)) + 3.*g_weight[NE]*rho*(g_cx[NE]*forceX + g_cy[NE]*forceY);
        comp[NE ] = shift;

        //===== NORTH WEST =====
        rho   = neigh(1,-1).density; 
        velX  = neigh(1,-1).velocityX + gx;
        velY  = neigh(1,-1).velocityY + gy;
		
        usq = (velX*velX + velY*velY); 
        double uxuyNW =  -velX + velY;
        rt2 = (1./36.)*rho;

        shift= omega_trm * GET_COMP(1,-1,NW) + omega * (rt2 * (1. + f1*uxuyNW + f2*uxuyNW*uxuyNW - f3*usq)) + 3.*g_weight[NW]*rho*(g_cx[NW]*forceX + g_cy[NW]*forceY);
        comp[NW] = shift;

        //===== SOUTH EAST =====
        rho   = neigh(-1,1).density; 
        velX  = neigh(-1,1).velocityX + gx;
        velY  = neigh(-1,1).velocityY + gy;
		
        usq = (velX*velX + velY*velY); 
        double uxuySE =  velX - velY;
        rt2 = (1./36.)*rho;

        shift = omega_trm * GET_COMP(-1,1,SE) + omega * (rt2 * (1. + f1*uxuySE + f2*uxuySE*uxuySE - f3*usq)) + 3.*g_weight[SE]*rho*(g_cx[SE]*forceX + g_cy[SE]*forceY);
        comp[SE ] = shift;

        //===== SOUTH WEST =====
        rho   = neigh(1,1).density; 
        velX  = neigh(1,1).velocityX + gx;
        velY  = neigh(1,1).velocityY + gy;
		
        usq = (velX*velX + velY*velY); 
        double uxuySW = -velX - velY;
        rt2 = (1./36.)*rho;

        shift = omega_trm * GET_COMP(1,1,SW) + omega * (rt2 * (1. + f1*uxuySW + f2*uxuySW*uxuySW - f3*usq)) + 3.*g_weight[SW]*rho*(g_cx[SW]*forceX + g_cy[SW]*forceY);
        comp[SW] = shift;
    }	 

    template<typename COORD_MAP>
    void BounceBackBoundaries( const COORD_MAP& neighborhood )
    {
        //               C  N  S  W  E NW NE SW SE
        int xstream[] = {0, 0, 0, 1,-1, 1,-1, 1,-1};
        int ystream[] = {0,-1, 1, 0, 0,-1,-1, 1, 1};		

        for(int i = 1; i < CELLSIZE; ++i ){
            if( GET_STATE(xstream[i],ystream[i]) == NO_SLIP ){
                comp[i] = GET_COMP(xstream[i],ystream[i],g_stream[i]);
            }
            else if( GET_STATE(xstream[i],ystream[i]) == OBSTACLE ){
                comp[i] = GET_COMP(xstream[i],ystream[i],g_stream[i]);
            }
            else if ( GET_STATE(xstream[i],ystream[i]) == ACCELERATION ){
                double tmp_ux = provider.GetTypeVelocityX("ACCELERATION");
                double tmp_uy = provider.GetTypeVelocityY("ACCELERATION");
                int dir = g_stream[i];
                comp[i] = GET_COMP( xstream[i], ystream[i], g_stream[i] ) + 6.0 * g_weight[dir] * ( xstream[dir]*tmp_ux + ystream[dir]*tmp_uy );
            }
            else if ( GET_STATE(xstream[i],ystream[i]) == FREE_SLIP_N_S ){
                comp[i] = GET_COMP(0,ystream[i],g_freeY[i]);
            }
            else if ( GET_STATE(xstream[i],ystream[i]) == FREE_SLIP_E_W ){
                comp[i] = GET_COMP(xstream[i],0,g_freeX[i]);
            }
	    else{			
                continue;
            }
        }

    }

    template<typename COORD_MAP>
    void calcMacroscopicValues( const COORD_MAP& neighborhood )
    {
        double rho  = 0.0;
        double velx = 0.0;
        double vely = 0.0;
        for(int i = 0; i < CELLSIZE; ++i ){
            rho  = rho + comp[i];
            velx = velx + g_cx[i]*comp[i];
            vely = vely + g_cy[i]*comp[i]; 
        }
        velocityX = velx * 1./rho; 
        velocityY = vely * 1./rho;
        density   = rho;
    }

    template<typename COORD_MAP>
    void ZouHeVelocityNorth ( const COORD_MAP& neighborhood )
    {
	double velY = provider.GetVelocityY("North");

	double velX = provider.GetVelocityX("North");
	double rho = comp[C] + comp[E] + comp[W] + 2.0*( comp[N] + comp[NE] + comp[NW] );
	rho = rho / (1.0+velY);
	double ruY = velY*velY;
        double ruX = rho * velX;
	
	comp[S ] = comp[ N] - ( (2.0)/(3.0) ) * ruY;
	comp[SW] = comp[NE] - ( (1.0)/(6.0) ) * ruY - 0.5*ruX + 0.5 * ( comp[E] - comp[W] );
	comp[SE] = comp[NW] - ( (1.0)/(6.0) ) * ruY + 0.5*ruX + 0.5 * ( comp[W] - comp[E] );

    }

    template<typename COORD_MAP>
    void ZouHeVelocitySouth( const COORD_MAP& neighborhood )
    {
	double velY = provider.GetVelocityY("South");

	double velX = provider.GetVelocityX("South");
	double rho = comp[C] + comp[E] + comp[W] + 2.0*( comp[S] + comp[SW] + comp[SE]);
	rho = rho / (1.0-velY);
	double ruY = rho * velY;
        double ruX = rho * velX;

	comp[N ] = comp[ S] + (2.0/3.0) * ruY;
	comp[NE] = comp[SW] + (1.0/6.0) * ruY + 0.5*ruX - 0.5 * ( comp[E] - comp[W] );
	comp[NW] = comp[SE] + (1.0/6.0) * ruY - 0.5*ruX - 0.5 * ( comp[W] - comp[E] );

    }
 
    template<typename COORD_MAP>
    void ZouHeVelocityWest( const COORD_MAP& neighborhood )
    {
	double velX = provider.GetVelocityX("West");

	double velY = provider.GetVelocityY("West");
	double rho = comp[C] + comp[N] + comp[S] + 2.0 * ( comp[W] + comp[SW] + comp[NW] );
    	rho = rho / (1.0 - velX);
    	double ruX = rho * velX;
        double ruY = rho * velY;

    	comp[E ] = comp[ W] + (2./3.) * ruX;
    	comp[NE] = comp[SW] + (1./6.) * ruX + 0.5*ruY - 0.5 * ( comp[N] - comp[S] );
    	comp[SE] = comp[NW] + (1./6.) * ruX - 0.5*ruY - 0.5 * ( comp[S] - comp[N] );
    }


    template<typename COORD_MAP>
    void ZouHeVelocityEast( const COORD_MAP& neighborhood )
    {
	double velX = provider.GetVelocityX("East");

	double velY = provider.GetVelocityY("East");
   	double rho = comp[C] + comp[N] + comp[S] + 2.0 * ( comp[E] + comp[NE] + comp[SE] );
    	rho = rho / (1.0 + velX);
    	double ruX = rho * velX;
        double ruY = rho * velY;

    	comp[W ] = comp[ E] - (2./3.) * ruX;
    	comp[SW] = comp[NE] - (1./6.) * ruX - 0.5*ruY + 0.5 * ( comp[N] - comp[S] );
	comp[NW] = comp[SE] - (1./6.) * ruX + 0.5*ruY + 0.5 * ( comp[S] - comp[N] );
		
    }

    template<typename COORD_MAP>
    void ZouHePressureEast( const COORD_MAP& neighborhood )
    {
	double rho = provider.GetPressure("East");
        
        double velY = provider.GetVelocityY("East");
    	double velX =  -1.+ ( comp[C] + comp[N] + comp[S] + 2.0*( comp[E] + comp[NE] + comp[SE] ) )/rho;
	double ruX = rho*velX;
	double ruY = rho*velY;
	
	comp[W ] = comp[E ] - (2./3.) * ruX;
	comp[SW] = comp[NE] - (1./6.) * ruX - 0.5*ruY + 0.5*( comp[N] - comp[S] );
	comp[NW] = comp[SE] - (1./6.) * ruX + 0.5*ruY + 0.5*( comp[S] - comp[N] );
		
    } 

    template<typename COORD_MAP>
    void ZouHePressureWest( const COORD_MAP& neighborhood )
    {
        double rho = provider.GetPressure("West");

        double velY = provider.GetVelocityY("West");
        double velX = 1.0 - ( comp[C] + comp[N] + comp[S] + 2.0*( comp[W] + comp[SW] + comp[NW] ) )/rho;
	double ruX = rho*velX;
        double ruY = rho*velY;

    	comp[E ] = comp[ W] + (2./3.) * ruX;
    	comp[NE] = comp[SW] + (1./6.) * ruX + 0.5*ruY - 0.5 * ( comp[N] - comp[S] );
    	comp[SE] = comp[NW] + (1./6.) * ruX - 0.5*ruY - 0.5 * ( comp[S] - comp[N] );
    }

    template<typename COORD_MAP>
    void ZouHePressureSouth( const COORD_MAP& neighborhood )
    {
        double rho  = provider.GetPressure("South");
 
	double velX = provider.GetVelocityX("South");
	double velY = 1.0 - (comp[C] + comp[E] + comp[W] + 2.0*( comp[S] + comp[SW] + comp[SE]) )/rho;
	double ruX = rho * velX;
        double ruY = rho * velY;

	comp[N ] = comp[ S] + (2.0/3.0) * ruY;
	comp[NE] = comp[SW] + (1.0/6.0) * ruY + 0.5*ruX - 0.5 * ( comp[E] - comp[W] );
	comp[NW] = comp[SE] + (1.0/6.0) * ruY - 0.5*ruX - 0.5 * ( comp[W] - comp[E] );
    }

    template<typename COORD_MAP>
    void ZouHePressureNorth ( const COORD_MAP& neighborhood )
    {
	double rho = provider.GetPressure("North");

	double velX = provider.GetVelocityX("North");
	double velY = -1.0 + (comp[C] + comp[E] + comp[W] + 2.0*( comp[N] + comp[NE] + comp[NW]) )/rho;
        double ruX = rho * velX;
	double ruY = rho * velY;
	
	comp[S ] = comp[ N] - ( (2.0)/(3.0) ) * ruY;
	comp[SW] = comp[NE] - ( (1.0)/(6.0) ) * ruY - 0.5*ruX + 0.5 * ( comp[E] - comp[W] );
	comp[SE] = comp[NW] - ( (1.0)/(6.0) ) * ruY + 0.5*ruX + 0.5 * ( comp[W] - comp[E] );

    }

    template<typename COORD_MAP>
    void ZouHeVelocityCornerSouthWest( const COORD_MAP& neighborhood )
    {
        double rho = neigh(0,1).density;
        double velX = provider.GetVelocityX("SouthWest");
        double velY = provider.GetVelocityY("SouthWest");
           
        double f1 = 2./3.;
        double f2 = 1./6.;
        double f3 = 1./12.;
        
        double ruX = velX*rho;
        double ruY = velY*rho;

        comp[E ] = comp[W ] + f1*ruX;
        comp[N ] = comp[S ] + f1*ruY;
        comp[NE] = comp[SW] + f2*(ruX+ruY);

        double tmp = 0.5 * (rho - ( comp[C]+comp[E]+comp[N]+comp[W]+comp[S]+comp[NE]+comp[SW] ) );

        comp[NW] = tmp + f3*(ruX-ruY);
        comp[SE] = tmp - f3*(ruX-ruY);

    }

    template<typename COORD_MAP>
    void ZouHeVelocityCornerSouthEast( const COORD_MAP& neighborhood )
    {

        double rho  = neigh(0,1).density;
        double velX = provider.GetVelocityX("SouthEast");
        double velY = provider.GetVelocityY("SouthEast");
           
        double f1 = 2./3.;
        double f2 = 1./6.;
        double f3 = 1./12.;
        
        double ruX = velX*rho;
        double ruY = velY*rho;

        comp[W ] = comp[E ] - f1*ruX ;
        comp[N ] = comp[S ] + f1*ruY;
        comp[NW] = comp[SE] - f2*(ruX-ruY);
        
        double tmp = 0.5 * (rho - ( comp[C]+comp[N]+comp[S]+comp[W]+comp[E]+comp[NW]+comp[SE] ) );
        comp[NE] = tmp + f3*(ruX+ruY);
        comp[SW] = tmp - f3*(ruX+ruY); 

    }

    template<typename COORD_MAP>
    void ZouHeVelocityCornerNorthWest( const COORD_MAP& neighborhood )
    {

        double rho = neigh(0,-1).density;
        double velX = provider.GetVelocityX("NorthWest");
        double velY = provider.GetVelocityY("NorthWest");
           
        double f1 = 2./3.;
        double f2 = 1./6.;
        double f3 = 1./12.;
        
        double ruX = velX*rho;
        double ruY = velY*rho;

        comp[E ] = comp[W ] + f1*ruX;
        comp[S ] = comp[N ] - f1*ruY;
        comp[SE] = comp[NW] + f2*(ruX-ruY);

        double tmp = 0.5 * (rho - ( comp[C]+comp[N]+comp[S]+comp[W]+comp[E]+comp[NW]+comp[SE] ) );
        comp[NE] = tmp + f3*(ruX+ruY);
        comp[SW] = tmp - f3*(ruX+ruY);  

    }

    template<typename COORD_MAP>
    void ZouHeVelocityCornerNorthEast( const COORD_MAP& neighborhood )
    {

        double rho =  neigh(0,-1).density;
        double velX = provider.GetVelocityX("NorthEast");
        double velY = provider.GetVelocityY("NorthEast");
        
        double f1 = 2./3.;
        double f2 = 1./6.;
        double f3 = 1./12.;
        
        double ruX = velX*rho;
        double ruY = velY*rho;

        comp[W ] = comp[E ] - f1*ruX;
        comp[S ] = comp[N ] - f1*ruY;
        comp[SW] = comp[NE] - f2*ruX - f2*ruY;
        
        double tmp  = 0.5 * (rho - ( comp[C]+comp[N]+comp[S]+comp[W]+comp[E]+comp[NE]+comp[SW] ) );
        comp[NW] = tmp - f3*(ruX-ruY);
        comp[SE] = tmp + f3*(ruX-ruY); 

    }


    template<typename COORD_MAP>
    void ZouHePressureCornerSouthWest( const COORD_MAP& neighborhood )
    {
        double rho = provider.GetPressure("SouthWest");
        double velX = provider.GetVelocityX("SouthWest");
        double velY = provider.GetVelocityY("SouthWest");
           
        double f1 = 2./3.;
        double f2 = 1./6.;
        double f3 = 1./12.;
        
        double ruX = velX*rho;
        double ruY = velY*rho;

        comp[E ] = comp[W ] + f1*ruX;
        comp[N ] = comp[S ] + f1*ruY;
        comp[NE] = comp[SW] + f2*(ruX+ruY);

        double tmp = 0.5 * (rho - ( comp[C]+comp[E]+comp[N]+comp[W]+comp[S]+comp[NE]+comp[SW] ) );

        comp[NW] = tmp + f3*(ruX-ruY);
        comp[SE] = tmp - f3*(ruX-ruY);

    }

    template<typename COORD_MAP>
    void ZouHePressureCornerSouthEast( const COORD_MAP& neighborhood )
    {

        double rho  = provider.GetPressure("SouthEast");
        double velX = provider.GetVelocityX("SouthEast");
        double velY = provider.GetVelocityY("SouthEast");
           
        double f1 = 2./3.;
        double f2 = 1./6.;
        double f3 = 1./12.;
        
        double ruX = velX*rho;
        double ruY = velY*rho;

        comp[W ] = comp[E ] - f1*ruX ;
        comp[N ] = comp[S ] + f1*ruY;
        comp[NW] = comp[SE] - f2*(ruX-ruY);
        
        double tmp = 0.5 * (rho - ( comp[C]+comp[N]+comp[S]+comp[W]+comp[E]+comp[NW]+comp[SE] ) );
        comp[NE] = tmp + f3*(ruX+ruY);
        comp[SW] = tmp - f3*(ruX+ruY); 

    }

    template<typename COORD_MAP>
    void ZouHePressureCornerNorthWest( const COORD_MAP& neighborhood )
    {

        double rho = provider.GetPressure("NorthWest");
        double velX = provider.GetVelocityX("NorthWest");
        double velY = provider.GetVelocityY("NorthWest");
           
        double f1 = 2./3.;
        double f2 = 1./6.;
        double f3 = 1./12.;
        
        double ruX = velX*rho;
        double ruY = velY*rho;

        comp[E ] = comp[W ] + f1*ruX;
        comp[S ] = comp[N ] - f1*ruY;
        comp[SE] = comp[NW] + f2*(ruX-ruY);

        double tmp = 0.5 * (rho - ( comp[C]+comp[N]+comp[S]+comp[W]+comp[E]+comp[NW]+comp[SE] ) );
        comp[NE] = tmp + f3*(ruX+ruY);
        comp[SW] = tmp - f3*(ruX+ruY);  

    }

    template<typename COORD_MAP>
    void ZouHePressureCornerNorthEast( const COORD_MAP& neighborhood )
    {

        double rho =  provider.GetPressure("NorthEast");
        double velX = provider.GetVelocityX("NorthEast");
        double velY = provider.GetVelocityY("NorthEast");
        
        double f1 = 2./3.;
        double f2 = 1./6.;
        double f3 = 1./12.;
        
        double ruX = velX*rho;
        double ruY = velY*rho;

        comp[W ] = comp[E ] - f1*ruX;
        comp[S ] = comp[N ] - f1*ruY;
        comp[SW] = comp[NE] - f2*ruX - f2*ruY;
        
        double tmp  = 0.5 * (rho - ( comp[C]+comp[N]+comp[S]+comp[W]+comp[E]+comp[NE]+comp[SW] ) );
        comp[NW] = tmp - f3*(ruX-ruY);
        comp[SE] = tmp + f3*(ruX-ruY); 

    }

    template<typename COORD_MAP>
    void GridExchange( const COORD_MAP& neighborhood )
    {
        state = GET_STATE(0,0);
        bounceback = neigh(0,0).bounceback;
        iter = neigh(0,0).iter;  
        velocityX = neigh(0,0).velocityX;
        velocityY = neigh(0,0).velocityY;
        density   = neigh(0,0).density;

        for(int i=0;i<CELLSIZE;++i){
            comp[i] = GET_COMP(0,0,i);
        }
    }

    int iter;
    double comp[CELLSIZE];
    double density;
    double velocityX;
    double velocityY;
    int posx;
    int posy;
    int bounceback;
    State state;
    static Provider provider;
};

Provider Cell::provider;
#endif
