#include <libgeodecomp/io/bovwriter.h>
#include <libgeodecomp/io/tracingwriter.h>
#include <libgeodecomp/loadbalancer/tracingbalancer.h>
#include <libgeodecomp/loadbalancer/noopbalancer.h>
#include <libgeodecomp/parallelization/stripingsimulator.h>

#include "../src/Cell.h"
#include "../src/CellInitializer.h"
#include "../../util/Provider.h"
#include "../../util/SelectorBOV.h"
#include "../../util/FileReader.h"
#include "../../util/Constants.h"
#include "../../util/StorageStructs.h"

using namespace LibGeoDecomp;

void SetBoundaries(FileReader* Reader, CellInitializer* cellinit)
{
    std::map<string,string> boundary;

    boundary["North"] = Reader->GetType("North") ;
    boundary["South"] = Reader->GetType("South") ;
    boundary["East" ] = Reader->GetType("East") ;
    boundary["West" ] = Reader->GetType("West") ;

    boundary["NorthEast"] = Reader->GetType("NorthEast") ;
    boundary["NorthWest"] = Reader->GetType("NorthWest") ;
    boundary["SouthEast"] = Reader->GetType("SouthEast") ;
    boundary["SouthWest"] = Reader->GetType("SouthWest") ;

    cellinit->SetBoundary(boundary);
}

void SetImage(FileReader* Reader, CellInitializer* cellinit)
{
    const string image = Reader->GetValue<string>("Image");
    cellinit->SetImage( image);
}

void SetObstacles(FileReader* Reader, CellInitializer* cellinit )
{
    vector<RectangleStorage> obstacleRectangle = Reader->GetRectangles();
    vector<CircleStorage> obstacleCircle = Reader->GetCircles();
    vector<TriangleStorage> obstacleTriangle = Reader->GetTriangles();

    std::vector<RectangleStorage>::iterator itRe; 
    for(itRe=obstacleRectangle.begin(); itRe != obstacleRectangle.end(); ++itRe){
         cellinit->SetRectangle(itRe->M_X,itRe->M_Y,itRe->l_X,itRe->l_Y);

    }
    std::vector<CircleStorage>::iterator itCi;
    for(itCi=obstacleCircle.begin(); itCi != obstacleCircle.end(); ++itCi){
        cellinit->SetCircle(itCi->M_X,itCi->M_Y,itCi->r);
    }
    std::vector<TriangleStorage>::iterator itTr;
    for(itTr=obstacleTriangle.begin(); itTr != obstacleTriangle.end(); ++itTr){
        cellinit->SetTriangle(itTr->P1_X,itTr->P1_Y,itTr->P2_X,itTr->P2_Y,itTr->P3_X,itTr->P3_Y);
    }
}

void SetWriter(StripingSimulator<Cell>* sim, FileReader* Reader)
{
    const string outputvelocity = Reader->GetValue<string>("OutputVelocity");
    const string outputdensity  = Reader->GetValue<string>("OutputDensity");
    const string outputflag = Reader->GetValue<string>("OutputFlag");    
    const int printstep = Reader->GetValue<int>("printstep");
    const int tracestep = Reader->GetValue<int>("tracestep");
    const int timesteps = Reader->GetValue<int>("timesteps");

    if(outputvelocity != "NULL" ){
        sim->addWriter(new BOVWriter<Cell, VelocitySelector>(outputvelocity+".velocity",printstep));
    }
    if(outputdensity != "NULL" ){
        sim->addWriter(new BOVWriter<Cell, DensitySelector>(outputdensity+".density",printstep));
    }
    if(outputflag != "NULL" ){
        sim->addWriter(new BOVWriter<Cell, FlagSelector>(outputflag+".flag",printstep));
    }

    if (MPILayer().rank() == 0){
        sim->addWriter(new TracingWriter<Cell>(tracestep, timesteps));
    }
}

void runSimulation(CellInitializer* cellinit, FileReader* Reader)
{
    MPI::Aint displacements[] = {0};
    MPI::Datatype memberTypes[] = {MPI::CHAR};
    int lengths[] = {sizeof(Cell)};
    MPI::Datatype objType;
    objType = MPI::Datatype::Create_struct(1, lengths, displacements, memberTypes);
    objType.Commit();

    StripingSimulator<Cell> sim( cellinit, MPILayer().rank() ? 0 : new TracingBalancer(new NoOpBalancer()), 100000, objType);
    SetWriter(&sim, Reader );
    sim.run();

} 

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);
    Typemaps::initializeMaps();
    
    string inputfile;

    if(argc != 2){
        cerr << "Missing Parameter File!" << endl;
        exit(1);
    }

    inputfile = argv[1];
    FileReader Reader;
    Reader.read( inputfile );

    const int sizeX = Reader.GetValue<int>("sizeX");
    const int sizeY = Reader.GetValue<int>("sizeY");
    const int timesteps = Reader.GetValue<int>("timesteps");

    
    CellInitializer *cellInit = new CellInitializer(Coord<2>( sizeX, sizeY), timesteps);
    SetBoundaries(&Reader, cellInit);
    SetObstacles(&Reader, cellInit);
    SetImage(&Reader, cellInit);

    runSimulation(cellInit, &Reader);
    MPI_Finalize();
    return 0;
}

