# ifndef _IMAGE_READER_H_
# define _IMAGE_READER_H_

#include <string.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

using namespace std;

class ImageReader{

private:
    int m_sizeX;
    int m_sizeY;
    vector<int> m_vals;

public:
    ImageReader( string image, int x, int y );
    ~ImageReader();

    void ReadPGM( string image );
    void ReadPGM_2( string image );
    void TestPGM( string test );

    int GetX();
    int GetY();

    int GetValue ( const int i, const int j );
    void SetValue( const int i, const int j, const int value );
    vector<int> GetAllValues();    
};

#endif

