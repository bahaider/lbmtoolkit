#ifndef _SELECTOR_VTK_H_
#define _SELECTOR_VTK_H_

class DensitySelector
{
public:
    typedef double VariableType;

    static std::string varName()
    {
        return "density";
    }

    static std::string dataFormat()
    {
        return "double 1";
    }
    static std::string dataType()
    {
	return "SCALARS";
    }

    static int dataComponents()
    {
        return 1;
    }

    void operator()(const Cell& cell, double *storage)
    {
         *storage = cell.density;
    }
};

class VelocitySelector
{
public:
    typedef double VariableType;

    static std::string varName()
    {
        return "velocity";
    }

    static std::string dataFormat()
    {
        return "double";
    }

    static std::string dataType()
    {
    	return "VECTORS";
    }

    static int dataComponents()
    {
        return 3;
    }

    void operator()(const Cell& cell, double *storage)
    {
         State s = cell.state;
         if(s == OBSTACLE || s == NO_SLIP){
            storage[0] = 0.0;
            storage[1] = 0.0;
            storage[2] = 0.0;
         }
         else{
            storage[0] = cell.velocityX;
            storage[1] = cell.velocityY;
            storage[2] = 0.0;
         }
    }
};

class FlagSelector
{
public:
    typedef double VariableType;

    static std::string varName()
    {
        return "flag";
    }

    static std::string dataFormat()
    {
        return "int 1";
    }
    static std::string dataType()
    {
	return "SCALARS";
    }

    static int dataComponents()
    {
        return 1;
    }

    void operator()(const Cell& cell, double *storage)
    {
         *storage = cell.state;
    }
};

#endif
