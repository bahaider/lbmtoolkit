#ifndef _RECTANGLE_H_
#define _RECTANGLE_H_

#include "Geometry.h"

class Rectangle : public Geometry
{
   private:
       double m_half_lX;
       double m_half_lY;
       double m_half_lX_B;
       double m_half_lY_B;
      
   public:
       Rectangle(int mx, int my, int lx, int ly) : Geometry(mx,my)
       {
           m_half_lX = lx/2;
           m_half_lY = ly/2;
           m_half_lX_B = (lx+2)/2;
           m_half_lY_B = (ly+2)/2;
                
       }
       bool IsInside(int x, int y)
       {
           int xn = abs(x-m_pX);
           int yn = abs(y-m_pY);
           if(xn <= m_half_lX && yn <= m_half_lY){
               return 1;
           }
           else{ return 0; }           
       }   
       bool IsBoundingBox(int x, int y)
       {
           int xn = abs(x-m_pX);
           int yn = abs(y-m_pY);
           if(xn <= m_half_lX_B && yn <= m_half_lY_B){
               return 1;
           }
           else{ return 0; }           
       }   

};
#endif

