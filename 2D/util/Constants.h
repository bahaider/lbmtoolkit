#ifndef CONFIG_H
#define CONFIG_H

#include<string>

// Describes the eight moving directions, the distribution functions are allowed to move in the D2Q9 Model
enum {
	C  = 0,
	N  = 1,
	S  = 2,
	W  = 3,
	E  = 4,
	NW = 5,
	NE = 6,
	SW = 7,
	SE = 8,
	CELLSIZE = 9 
};

// Defines the state of the Cells
enum State {
	FLUID                      = 0, 
	NO_SLIP                    = 1,
        OBSTACLE                   = 2,
	FREE_SLIP_N_S              = 3,
        FREE_SLIP_E_W              = 4,
	ACCELERATION               = 5, 
	ZOU_HE_EAST_VELOCITY       = 6, 
	ZOU_HE_WEST_VELOCITY       = 7, 
	ZOU_HE_NORTH_VELOCITY      = 8, 
	ZOU_HE_SOUTH_VELOCITY      = 9,
	ZOU_HE_EAST_PRESSURE       = 10, 
	ZOU_HE_WEST_PRESSURE       = 11, 
	ZOU_HE_NORTH_PRESSURE      = 12, 
	ZOU_HE_SOUTH_PRESSURE      = 13,
        CORNER_NORTH_EAST_VELOCITY = 14,
        CORNER_NORTH_WEST_VELOCITY = 15,
        CORNER_SOUTH_EAST_VELOCITY = 16,
        CORNER_SOUTH_WEST_VELOCITY = 17,
        CORNER_NORTH_EAST_PRESSURE = 18,
        CORNER_NORTH_WEST_PRESSURE = 19,
        CORNER_SOUTH_EAST_PRESSURE = 20,
        CORNER_SOUTH_WEST_PRESSURE = 21,
	DUMMY                      = 22
};


//                         C  N   S   W  E   NW  NE  SW  SE
const int g_cx[]       = { 0, 0,  0, -1, 1, -1,  1, -1,  1 };
const int g_cy[]       = { 0, 1, -1,  0, 0,  1,  1, -1, -1 };
const int g_stream[]   = { C, S,  N,  E, W, SE, SW, NE, NW }; // Destination Cell
const int g_freeX[]    = { C, N,  S,  E, W, NE, NW, SE, SW };
const int g_freeY[]    = { C, S,  N,  W, E, SW, SE, NW, NE };


// The Equilibrium Distribution weights of the D2Q9 Model
const double g_weight[] = {4./9 ,
                           1./9 , 1./9 , 1./9 , 1./9 ,
                           1./36, 1./36, 1./36, 1./36};

// Path of the parameter file
const std::string g_filename = "../input/params.txt";


#endif

