#ifndef _PROVIDER_H_
#define _PROVIDER_H_

#include"FileReader.h"
#include"Constants.h"
#include <vector>

class Provider
{
private:
    double m_dx;
    double m_dt;
    double m_tau; 
    
    double m_nueP; // Si Value
    double m_nueL; // Lattice Value
    
    
    int m_timesteps;
    int m_size[2]; 

    double m_pressureIn;
    double m_pressureOut;
	
    double m_pressure;
    double m_initVel[2]; 

    double m_gravityX;
    double m_gravityY;
    double m_forceX;
    double m_forceY;
  
    map<std::string,BoundaryStorage>* m_boundaryVals;
public:
    Provider();
    ~Provider();
    
//=====================     
//  Access functions:
//=====================

    double GetDx();
    double GetDt();
    double GetTau();
    
    double GetNue_L();
    double GetNue_P();

    double GetGravityX();
    double GetGravityY();

    double GetForceX();
    double GetForceY();
    
    double GetVelocityX( string key );
    double GetTypeVelocityX( string type );

    double GetVelocityY( string key );
    double GetTypeVelocityY( string type );

    double GetPressure( string key);
    double GetTypePressure( string type );

    double GetInitVelocity_L( char c );
	
    double GetPressure();	

    int GetSize( char c );    
    int GetTimesteps();

    double GetPressureIn_L();
    double GetPressureOut_L();

//==================== 	
//  Util stuff Pt 1:
//====================

    double GetSimulationTime();
    double GetSimulationLengthX();
    double GetSimulationLengthY();
   
    double CalcOmega();  

private:
    void InitValues();
    
//====================
//   Util stuff Pt 2
//====================
    void CalcNue_L();
    void CalcNue_P();
    
};

#endif
