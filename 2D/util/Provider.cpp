#include"Provider.h"
#include<string>
#include "Constants.h"

using namespace std;


Provider::Provider()
{
    InitValues();
}

Provider::~Provider()
{
    
}

void Provider::InitValues()
{
    FileReader Reader;
    Reader.read( g_filename );
  
    m_timesteps = Reader.GetValue<int>( "timesteps" );
    m_size[0] = Reader.GetValue<int>( "sizeX" );
    m_size[1] = Reader.GetValue<int>( "sizeY" );
	
    m_dt = Reader.GetValue<double>( "dt" );
    m_dx = Reader.GetValue<double>( "dx" );
    m_tau = Reader.GetValue<double>( "tau" );
    if( (m_tau < 0.5) || (m_tau > 2.0) ) cerr << "WARNING: Wrong tau: " << m_tau << endl;
    
    m_pressure = Reader.GetValue<double>( "initPressure" );
	
    m_initVel[0] = Reader.GetValue<double>( "initVelocityX_L" );
    m_initVel[1] = Reader.GetValue<double>( "initVelocityY_L" ); 

    m_gravityX = Reader.GetValue<double>( "GravityX" );
    m_gravityY = Reader.GetValue<double>( "GravityY" );

    m_forceX = Reader.GetValue<double>( "ForceX" );
    m_forceY = Reader.GetValue<double>( "ForceY" );

    m_boundaryVals = new map<string,BoundaryStorage>();
 
    BoundaryStorage tmp = Reader.GetStruct("North");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("North",tmp));
    
    tmp = Reader.GetStruct("South");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("South",tmp));
     
    tmp = Reader.GetStruct("East");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("East",tmp));

    tmp = Reader.GetStruct("West");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("West",tmp));

    tmp = Reader.GetStruct("NorthWest");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("NorthWest",tmp));

    tmp = Reader.GetStruct("NorthEast");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("NorthEast",tmp));

    tmp = Reader.GetStruct("SouthWest");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("SouthWest",tmp));

    tmp = Reader.GetStruct("SouthEast");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("SouthEast",tmp));

}

//====================
//   Util Stuff pt 2
//====================
void Provider::CalcNue_L()
{
    m_nueL = (2.0 * m_tau -1.0)/6.0;
}

void Provider::CalcNue_P()
{
    m_nueP = (m_nueL*m_dx*m_dx)/m_dt;
}

//=======================
//    Access Functions
//=======================
double Provider::GetDx()
{
    return m_dx;
}

double Provider::GetPressure()
{
    return m_pressure;
}
double Provider::GetDt()
{
    return m_dt;
}

double Provider::GetGravityX()
{
    return m_gravityX;
}

double Provider::GetGravityY()
{
    return m_gravityY;
}

double Provider::GetForceX()
{
    return m_forceX;
}

double Provider::GetForceY()
{
    return m_forceY;
}

int Provider::GetSize( char c )
{
    if( c == 'x' ){
        return m_size[0];
    }
    else if( c == 'y' ){
        return m_size[1]; 
    }
    else{
	std::cerr << "Something strange in the neighborhood! Need help call the Ghostbusters!! " << "#### Wrong call of Provider::GetSize( c ) ### " << std::endl;
	return 10;  
    }	
}

double Provider::GetPressureIn_L()
{
    return m_pressureIn;
}

double Provider::GetPressureOut_L()
{
    return m_pressureOut;
}

int Provider::GetTimesteps()
{
    return m_timesteps;
}
double Provider::GetTau()
{
    return m_tau;
}

double Provider::GetNue_L()
{
    return m_nueL;
}

double Provider::GetNue_P()
{
    return m_nueP;
}

double Provider::GetVelocityX(string key)
{

    map<string,BoundaryStorage>::iterator it;
    it = m_boundaryVals->find(key);
    if(it == m_boundaryVals->end()){
        std::cerr << "Provider: --> Wrong Key --> "+key << std::endl;
        exit(1);
    }
    return it->second.velocityX;

return 0.0;
}

double Provider::GetTypeVelocityX(string type)
{
    map<string,BoundaryStorage>::iterator it;
    for(it=m_boundaryVals->begin();it != m_boundaryVals->end();++it){
        if(it->second.type == type){
            return it->second.velocityX;     
        }
    }
return 0.0;
}

double Provider::GetVelocityY(string key)
{

    map<string,BoundaryStorage>::iterator it;
    it = m_boundaryVals->find(key);
    if(it == m_boundaryVals->end()){
        std::cerr << "Provider: --> Wrong Key --> "+key << std::endl;
        exit(1);
    }
    return it->second.velocityY;

return 0.0;
}

double Provider::GetTypeVelocityY(string type)
{
    map<string,BoundaryStorage>::iterator it;
    for(it=m_boundaryVals->begin();it != m_boundaryVals->end();++it){
        if(it->second.type == type){
            return it->second.velocityY;     
        }
    }
return 0.0;
}

double Provider::GetPressure(string key)
{

    map<string,BoundaryStorage>::iterator it;
    it = m_boundaryVals->find(key);
    if(it == m_boundaryVals->end()){
        std::cerr << "Provider: --> Wrong Key --> "+key << std::endl;
        exit(1);
    }
    return it->second.pressure;

return 1.0;
}

double Provider::GetTypePressure(string type)
{
    map<string,BoundaryStorage>::iterator it;
    for(it=m_boundaryVals->begin();it != m_boundaryVals->end();++it){
        if(it->second.type == type){
            return it->second.pressure;     
        }
    }
return 1.0;
}

double Provider::GetInitVelocity_L( char c )
{
    if(c == 'x') return m_initVel[0];
    if(c == 'y') return m_initVel[1];
    cerr << "Warning:Lattice InitVelocity!!" << std::endl;
    return 0.0;
}

//====================
//  Util Stuff pt 1
//====================
double Provider::GetSimulationTime()
{
    return m_timesteps * m_dt;
}

double Provider::GetSimulationLengthX()
{
    return m_size[0]*m_dx;
}

double Provider::GetSimulationLengthY()
{
    return m_size[1]*m_dx;
}

double Provider::CalcOmega()
{
    return (1.0)/m_tau;
}

