#ifndef _FILEREADER_H_
#define _FILEREADER_H_

#include <map>
#include <vector>
#include <string>
#include <cstring>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <sstream>

#include "StorageStructs.h"

using namespace std;

class FileReader{

private:
    map<string,string> m_tablet;
    map<string,BoundaryStorage> m_tablet_2;
    vector<RectangleStorage> m_storageRectangle;
    vector<CircleStorage> m_storageCircle;
    vector<TriangleStorage> m_storageTriangle;
public:
    void read(string in);
    void PrintError(string message);

    BoundaryStorage GetStruct(const string key);
    template<typename T>
    T GetValue(const string key);
    string GetType(const string key);
    
    vector<RectangleStorage> GetRectangles();
    vector<TriangleStorage>  GetTriangles();
    vector<CircleStorage>    GetCircles();
  
};

inline void FileReader::read(string f_in)
{
    char line[80];
    string first,second;
    double ux, uy, p;
    ifstream input;
    input.open(f_in.c_str(), ifstream::in);
    
    if(input.is_open()){

        while( input.getline(line,80)){
            if(strlen(line) == 0 || line[0] == '#') continue;
            istringstream istr(line);
            if (istr >> first ){
                if(first.find("North") != string::npos || first.find("South") != string::npos || first.find("East")!=string::npos || first.find("West")!=string::npos ){
                    if(istr >> second >> ux >> uy >> p){
                        BoundaryStorage tmp = {second,ux,uy,p};
                        m_tablet_2[first.c_str()] = tmp; 
                    }
                    else{
                        BoundaryStorage tmp2 = {second,0.0,0.0,1.0};
                        m_tablet_2[first.c_str()] = tmp2;
                    }
                }
                else if (first == "Triangle"){
                    double P1_x,P1_y,P2_x,P2_y,P3_x,P3_y;
                    if(istr >> P1_x >> P1_y >> P2_x >> P2_y >> P3_x >> P3_y){
                        TriangleStorage tmpTr = {P1_x,P1_y,P2_x,P2_y,P3_x,P3_y};
                        m_storageTriangle.push_back(tmpTr);        
                    }
                    else{
                        PrintError("Triangle wrong ");
                    }
                }
                else if(first == "Rectangle"){
                    double midRe_x,midRe_y,l_x,l_y;
                    if(istr >> midRe_x >> midRe_y >> l_x >> l_y){
                        RectangleStorage tmpRe = {midRe_x,midRe_y,l_x,l_y};
                        m_storageRectangle.push_back(tmpRe); 
                    }
                    else{
                        PrintError("Rectangle wrong ");
                    }
                }
                else if(first == "Circle"){
                    double midCi_x, midCi_y, r;
                    if(istr >> midCi_x >> midCi_y >> r){
                        CircleStorage tmpCi = {midCi_x,midCi_y,r};
                        m_storageCircle.push_back(tmpCi);
                    }
                    else{
                        PrintError("Circle wrong");
                    }
                } 
                else{
                    if(istr >> second){
                        m_tablet[first.c_str()] = second.c_str();
                    }
                    else{
                        PrintError(first);
                    }
                }
            }
            else{
                PrintError(first);
            }
        
        }
    }
    else{
        PrintError("OpeningFile");
    }
    input.close();
}

inline void FileReader::PrintError(string message)
{
    cerr << "ERROR: Parameterfile -> " << message << endl;
    exit(1);
}

inline BoundaryStorage FileReader::GetStruct(const string key )
{
    map<string,BoundaryStorage>::iterator it;

    it = m_tablet_2.find(key);
    if(it == m_tablet_2.end()){
        PrintError(key);
    }
    return it->second;
}

inline string FileReader::GetType(const string key)
{
    map<string,BoundaryStorage>::iterator it;

    it = m_tablet_2.find(key);
    if(it == m_tablet_2.end()){
        PrintError(key);
    }
    return it->second.type;
}

template<typename T>
T FileReader::GetValue( const string key )
{
    T val;
    map<string,string>::iterator it;
  
    it=m_tablet.find(key);
    if (it == m_tablet.end()) {
        cerr << "Parameter File not correct Missing / False  Value: " << key << endl;
        exit(0);
    }
    stringstream convert(it->second);
    convert >> val;
    return val;
}

inline vector<RectangleStorage> FileReader::GetRectangles()
{
    return m_storageRectangle;
}

inline vector<CircleStorage> FileReader::GetCircles()
{
    return m_storageCircle;
}

inline vector<TriangleStorage> FileReader::GetTriangles()
{
   return m_storageTriangle;
}

#endif
