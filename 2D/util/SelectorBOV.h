#ifndef _SELECTOR_BOV_H_
#define _SELECTOR_BOV_H_

#include "Constants.h"

class DensitySelector
{
public:
    typedef double VariableType;

    static std::string varName()
    {
        return "density";
    }

    static std::string dataFormat()
    {
        return "DOUBLE";
    }

    static int dataComponents()
    {
        return 1;
    }

    void operator()(const Cell& cell, double *storage)
    {
         *storage = cell.density;
    }
};

class VelocitySelector
{
public:
    typedef double VariableType;

    static std::string varName()
    {
        return "velocity";
    }

    static std::string dataFormat()
    {
        return "DOUBLE";
    }

    static int dataComponents()
    {
        return 2;
    }

    void operator()(const Cell& cell, double *storage)
    {
         State s = cell.state;
         if(s == OBSTACLE || s == NO_SLIP){
            storage[0] = 0.0;
            storage[1] = 0.0;
         }
         else{
            storage[0] = cell.velocityX;
            storage[1] = cell.velocityY;
         }
    }
};

class FlagSelector
{
public:
    typedef int  VariableType;

    static std::string varName()
    {
        return "flag";
    }

    static std::string dataFormat()
    {
        return "INT";
    }

    static int dataComponents()
    {
        return 1;
    }
 
    void operator()(const Cell& cell, int *storage)
    {
        *storage = cell.state;
    }   
};

#endif
